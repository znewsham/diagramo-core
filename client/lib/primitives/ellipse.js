/**
 * Approximate an ellipse through 4 bezier curves, one for each quadrant
 *
 * @constructor
 * @this {Ellipse}
 * @param {Point} centerPoint - the center point of the ellipse
 * @param {Number} width - the width of the ellipse
 * @param {Number} height - the height of the ellipse

 * @see <a href="http://www.codeguru.com/cpp/g-m/gdi/article.php/c131">http://www.codeguru.com/cpp/g-m/gdi/article.php/c131</a>
 * @see <a href="http://www.tinaja.com/glib/ellipse4.pdf">http://www.tinaja.com/glib/ellipse4.pdf</a>
 * @author Zack Newsham <zack_newsham@yahoo.co.uk>
 **/
function Ellipse(centerPoint, width, height) {
    DIAGRAMO.Primitive.call(this);
    /**"THE" constant*/
    var EToBConst = 0.2761423749154;

    /**Width offset*/
    var offsetWidth = width * 2 * EToBConst;

    /**Height offset*/
    var offsetHeight = height * 2 * EToBConst;

    /**The center {@linkDIAGRAMO.Point}*/
    this.centerPoint = centerPoint;

    /**Top left {@link DIAGRAMO.CubicCurve}*/
    this.topLeftCurve = new DIAGRAMO.CubicCurve(new DIAGRAMO.Point(centerPoint.x-width,centerPoint.y),new DIAGRAMO.Point(centerPoint.x-width,centerPoint.y-offsetHeight),new DIAGRAMO.Point(centerPoint.x-offsetWidth,centerPoint.y-height),new DIAGRAMO.Point(centerPoint.x,centerPoint.y-height));

    /**Top right {@link DIAGRAMO.CubicCurve}*/
    this.topRightCurve = new DIAGRAMO.CubicCurve(new DIAGRAMO.Point(centerPoint.x,centerPoint.y-height),new DIAGRAMO.Point(centerPoint.x+offsetWidth,centerPoint.y-height),new DIAGRAMO.Point(centerPoint.x+width,centerPoint.y-offsetHeight),new DIAGRAMO.Point(centerPoint.x+width,centerPoint.y));

    /**Bottom right {@link DIAGRAMO.CubicCurve}*/
    this.bottomRightCurve = new DIAGRAMO.CubicCurve(new DIAGRAMO.Point(centerPoint.x+width,centerPoint.y),new DIAGRAMO.Point(centerPoint.x+width,centerPoint.y+offsetHeight),new DIAGRAMO.Point(centerPoint.x+offsetWidth,centerPoint.y+height),new DIAGRAMO.Point(centerPoint.x,centerPoint.y+height));

    /**Bottom left {@link DIAGRAMO.CubicCurve}*/
    this.bottomLeftCurve = new DIAGRAMO.CubicCurve(new DIAGRAMO.Point(centerPoint.x,centerPoint.y+height),new DIAGRAMO.Point(centerPoint.x-offsetWidth,centerPoint.y+height),new DIAGRAMO.Point(centerPoint.x-width,centerPoint.y+offsetHeight),new DIAGRAMO.Point(centerPoint.x-width,centerPoint.y));

    /**The matrix array*/
    this.matrix = null; //TODO: do we really need this?

    /**The {@link DIAGRAMO.Style} used*/
    this.style = new DIAGRAMO.Style();
    this.style.gradientBounds = this.getBounds();

    /**Oject type used for JSON deserialization*/
    this.oType = 'Ellipse';
}
DIAGRAMO.Ellipse = Ellipse;

/**Creates a new {Ellipse} out of JSON parsed object
 *@param {JSONObject} o - the JSON parsed object
 *@return {Ellipse} a newly constructed DIAGRAMO.Ellipse
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
Ellipse.load = function(o){
    var newEllipse= new DIAGRAMO.Ellipse(new DIAGRAMO.Point(0,0), 0, 0); //fake ellipse (if we use a null centerPoint we got errors)

    newEllipse.offsetWidth = o.offsetWidth;
    newEllipse.offsetHeight = o.offsetHeight;
    newEllipse.centerPoint = DIAGRAMO.Point.load(o.centerPoint);
    newEllipse.topLeftCurve = DIAGRAMO.CubicCurve.load(o.topLeftCurve);
    newEllipse.topRightCurve = DIAGRAMO.CubicCurve.load(o.topRightCurve);
    newEllipse.bottomRightCurve = DIAGRAMO.CubicCurve.load(o.bottomRightCurve);
    newEllipse.bottomLeftCurve = DIAGRAMO.CubicCurve.load(o.bottomLeftCurve);
    this.matrix = DIAGRAMO.Matrix.clone(o.matrix);
    newEllipse.style = DIAGRAMO.Style.load(o.style);
    DIAGRAMO.Primitive.load(o, newEllipse);

    return newEllipse;
}


_.extend(Ellipse.prototype,DIAGRAMO.Primitive.prototype, {
    toJSONObject: function(){
        var ret = _.extend({}, DIAGRAMO.PointBasedPrimitive.prototype.toJSONObject.call(this),{
            oType: "DIAGRAMO.Ellipse",
            offsetWidth: this.offsetWidth,
            offsetHeight: this.offsetHeight,
            centerPoint: this.centerPoint.toJSONObject(),
            topLeftCurve: this.topLeftCurve.toJSONObject(),
            topRightCurve: this.topRightCurve.toJSONObject(),
            bottomLeftCurve: this.bottomLeftCurve.toJSONObject(),
            bottomRightCurve: this.bottomRightCurve.toJSONObject(),
        });
        return ret;
    },

    transform:function(matrix){
        this.topLeftCurve.transform(matrix);
        this.topRightCurve.transform(matrix);
        this.bottomLeftCurve.transform(matrix);
        this.bottomRightCurve.transform(matrix);
        this.centerPoint.transform(matrix);
        if(this.style){
            this.style.transform(matrix);
        }
    },

    paint:function(context){
        if(this.trigger("beforePaint")){
          return;
        }
        context.save();
        if(this.style!=null){
            this.style.setupContext(context);
        }
        context.beginPath();
        context.moveTo(this.topLeftCurve.startPoint.x, this.topLeftCurve.startPoint.y);
        context.bezierCurveTo(this.topLeftCurve.controlPoint1.x,
            this.topLeftCurve.controlPoint1.y, this.topLeftCurve.controlPoint2.x,
            this.topLeftCurve.controlPoint2.y, this.topLeftCurve.endPoint.x,
            this.topLeftCurve.endPoint.y);
        context.bezierCurveTo(this.topRightCurve.controlPoint1.x,
            this.topRightCurve.controlPoint1.y, this.topRightCurve.controlPoint2.x,
            this.topRightCurve.controlPoint2.y, this.topRightCurve.endPoint.x,
            this.topRightCurve.endPoint.y);
        context.bezierCurveTo(this.bottomRightCurve.controlPoint1.x,
            this.bottomRightCurve.controlPoint1.y, this.bottomRightCurve.controlPoint2.x,
            this.bottomRightCurve.controlPoint2.y, this.bottomRightCurve.endPoint.x,
            this.bottomRightCurve.endPoint.y);
        context.bezierCurveTo(this.bottomLeftCurve.controlPoint1.x,
            this.bottomLeftCurve.controlPoint1.y, this.bottomLeftCurve.controlPoint2.x,
            this.bottomLeftCurve.controlPoint2.y, this.bottomLeftCurve.endPoint.x,
            this.bottomLeftCurve.endPoint.y);

        //first fill
        if(this.style.fillStyle!=null && this.style.fillStyle!=""){
            context.fill();
        }

        //then stroke
        if(this.style.strokeStyle!=null && this.style.strokeStyle!=""){
            context.stroke();
        }
        context.restore();
    },

    contains:function(x,y){
        var points = this.topLeftCurve.getPoints();
        var curves = [this.topRightCurve, this.bottomRightCurve, this.bottomLeftCurve];
        for(var i=0; i<curves.length; i++){
            var curPoints = curves[i].getPoints();

            for(var a=0; a<curPoints.length; a++){
                points.push(curPoints[a]);
            }
        }
        return DIAGRAMO.Util.isPointInside(new DIAGRAMO.Point(x,y), points);

        return false;
    },

    near:function(x,y,radius){
        return this.topLeftCurve.near(x,y,radius) || this.topRightCurve.near(x,y,radius) || this.bottomLeftCurve.near(x,y,radius) || this.bottomRightCurve.near(x,y,radius);
    },

    equals:function(anotherEllipse){
        if(!anotherEllipse instanceof DIAGRAMO.Ellipse){
            return false;
        }

        return this.offsetWidth == anotherEllipse.offsetWidth
        && this.offsetHeight == anotherEllipse.offsetHeight
        && this.centerPoint.equals(anotherEllipse.centerPoint)
        && this.topLeftCurve.equals(anotherEllipse.topLeftCurve)
        && this.topRightCurve.equals(anotherEllipse.topRightCurve)
        && this.bottomRightCurve.equals(anotherEllipse.bottomRightCurve)
        && this.bottomLeftCurve.equals(anotherEllipse.bottomLeftCurve);
    //TODO: add this && this.matrix.equals(anotherEllipse.bottomLeftCurve)
    //TODO: add this && this.style.equals(anotherEllipse.bottomLeftCurve)
    },

    clone:function(){
        var ret=new DIAGRAMO.Ellipse(this.centerPoint.clone(),10,10);
        ret.topLeftCurve=this.topLeftCurve.clone();
        ret.topRightCurve=this.topRightCurve.clone();
        ret.bottomLeftCurve=this.bottomLeftCurve.clone();
        ret.bottomRightCurve=this.bottomRightCurve.clone();
        ret.style=this.style.clone();
        return ret;
    },

    toString:function(){
        return 'ellipse('+this.centerPoint+","+this.xRadius+","+this.yRadius+")";
    },

    /**
     *@see <a href="http://www.w3.org/TR/SVG/paths.html#PathDataCubicBezierCommands">http://www.w3.org/TR/SVG/paths.html#PathDataCubicBezierCommands</a>
     *@author Alex Gheorghiu <scriptoid.com>
     **/
    toSVG: function(){
        var result = "\n" + repeat("\t", INDENTATION) +  '<path d="M';
        result += this.topLeftCurve.startPoint.x + ',' + this.topLeftCurve.startPoint.y;

        //top left curve
        result += ' C' + this.topLeftCurve.controlPoint1.x + ',' + this.topLeftCurve.controlPoint1.y;
        result += ' ' + this.topLeftCurve.controlPoint2.x + ',' + this.topLeftCurve.controlPoint2.y;
        result += ' ' + this.topLeftCurve.endPoint.x + ',' + this.topLeftCurve.endPoint.y;

        //top right curve
        result += ' C' + this.topRightCurve.controlPoint1.x + ',' + this.topRightCurve.controlPoint1.y;
        result += ' ' + this.topRightCurve.controlPoint2.x + ',' + this.topRightCurve.controlPoint2.y;
        result += ' ' + this.topRightCurve.endPoint.x + ',' + this.topRightCurve.endPoint.y;

        //bottom right curve
        result += ' C' + this.bottomRightCurve.controlPoint1.x + ',' + this.bottomRightCurve.controlPoint1.y;
        result += ' ' + this.bottomRightCurve.controlPoint2.x + ',' + this.bottomRightCurve.controlPoint2.y;
        result += ' ' + this.bottomRightCurve.endPoint.x + ',' + this.bottomRightCurve.endPoint.y;

        //bottom left curve
        result += ' C' + this.bottomLeftCurve.controlPoint1.x + ',' + this.bottomLeftCurve.controlPoint1.y;
        result += ' ' + this.bottomLeftCurve.controlPoint2.x + ',' + this.bottomLeftCurve.controlPoint2.y;
        result += ' ' + this.bottomLeftCurve.endPoint.x + ',' + this.bottomLeftCurve.endPoint.y;

        result += '" ' + this.style.toSVG() +  '  />';
        return result;

    },


    getPoints:function(){
        var points = [];
        var curves = [this.topLeftCurve, this.topRightCurve,this.bottomRightCurve,this.bottomLeftCurve];

        for(var i=0; i<curves.length; i++){
            var curPoints = curves[i].getPoints();
            for(var a=0; a<curPoints.length; a++){
                points.push(curPoints[a]);
            }
        }
        return points;
    },


    getBounds:function(){
        return DIAGRAMO.Util.getBounds(this.getPoints());
    }
});
