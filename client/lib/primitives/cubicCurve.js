
/**
  * Creates an instance of a cubic curve.
  * A curved line determined by 2 normal points (startPoint and endPoint) and 2 control points (controlPoint1, controlPoint2)
  *
  * @constructor
  * @this {CubicCurve}
  * @param {Point} startPoint - starting point of the line
  * @param {Point} controlPoint1 - 1st control point of the line
  * @param {Point} controlPoint2 - 2nd control point of the line
  * @param {Point} endPoint - the ending point of the line
  * @see <a href="http://en.wikipedia.org/wiki/B%C3%A9zier_curve#Cubic_B.C3.A9zier_curves">http://en.wikipedia.org/wiki/B%C3%A9zier_curve#Cubic_B.C3.A9zier_curves</a>
 **/
function CubicCurve(startPoint, controlPoint1, controlPoint2, endPoint){
    DIAGRAMO.Curve.call(this);
    /**The start {@link DIAGRAMO.Point}*/
    this.startPoint = startPoint;
    this._cachedPoints = {};

    /**The first controll {@link DIAGRAMO.Point}*/
    this.controlPoint1 = controlPoint1;

    /**The second controll {@link DIAGRAMO.Point}*/
    this.controlPoint2 = controlPoint2;

    /**The end {@link DIAGRAMO.Point}*/
    this.endPoint = endPoint;

    /**The {@link DIAGRAMO.Style} of the quad*/
    this.style = new DIAGRAMO.Style();
    this.style.gradientBounds = this.getBounds();

    /**Object type used for JSON deserialization*/
    this.oType = 'CubicCurve';

}
DIAGRAMO.CubicCurve = CubicCurve;
/**Creates a {CubicCurve} out of JSON parsed object
 *@param {JSONObject} o - the JSON parsed object
 *@return {CubicCurve} a newly constructed DIAGRAMO.CubicCurve
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
CubicCurve.load = function(o){
    var newCubic = new DIAGRAMO.CubicCurve(
        DIAGRAMO.Point.load(o.startPoint),
        DIAGRAMO.Point.load(o.controlPoint1),
        DIAGRAMO.Point.load(o.controlPoint2),
        DIAGRAMO.Point.load(o.endPoint)
    );

    newCubic.style = DIAGRAMO.Style.load(o.style);
    DIAGRAMO.Primitive.load(o, newCubic);
    return newCubic;
}

CubicCurve.REFRESH_DISTANCE = 1.0;


_.extend(CubicCurve.prototype, DIAGRAMO.Curve.prototype,{
    toJSONObject: function(){
        var ret = _.extend({}, DIAGRAMO.PointBasedPrimitive.prototype.toJSONObject.call(this),{
            oType: "DIAGRAMO.CubicCurve",
            startPoint: this.startPoint.toJSONObject(),
            controlPoint1: this.controlPoint1.toJSONObject(),
            controlPoint2: this.controlPoint2.toJSONObject(),
            endPoint: this.endPoint.toJSONObject()
        });
        return ret;
    },
    transform:function(matrix){
        if(this.style != null){
            this.style.transform(matrix);
        }
        this.startPoint.transform(matrix);
        this.controlPoint1.transform(matrix);
        this.controlPoint2.transform(matrix);
        this.endPoint.transform(matrix);
    },


    paint:function(context){
      if(this.trigger("beforePaint")){
        return;
      }
      context.save();
		context.beginPath();

//        Log.group("CubicCurve:paint() ");
        if(this.style != null){
            this.style.setupContext(context);
        }

        context.beginPath();
        context.moveTo(this.startPoint.x, this.startPoint.y);
        context.bezierCurveTo(
            this.controlPoint1.x, this.controlPoint1.y,
            this.controlPoint2.x, this.controlPoint2.y,
            this.endPoint.x, this.endPoint.y
        );


        if(this.style.fillStyle != null && this.style.fillStyle != ""){
//            Log.info("Fill provided");
            context.fill();
        }
        else{
//            Log.info("Fill NOT provided")
        }

        if(this.style.strokeStyle != null && this.style.strokeStyle != ""){
//            Log.info("Stroke provided");
            context.stroke();
        }
        else{
//            Log.info("Stroke NOT provided")
        }
        context.restore();

    },


    clone:function(){
        var ret = new DIAGRAMO.CubicCurve(this.startPoint.clone(),this.controlPoint1.clone(), this.controlPoint2.clone(),this.endPoint.clone());
        ret.style = this.style.clone();
        return ret;
    },


    equals:function(anotherCubicCurve){
        if(!anotherCubicCurve instanceof DIAGRAMO.CubicCurve){
            return false;
        }
        return this.startPoint.equals(anotherCubicCurve.startPoint)
        && this.controlPoint1.equals(anotherCubicCurve.controlPoint1)
        && this.controlPoint2.equals(anotherCubicCurve.controlPoint2)
        && this.endPoint.equals(anotherCubicCurve.endPoint);
    },


    refreshPoints: function(figurePoints, points){
        return DIAGRAMO.Util.distance(this.startPoint, figurePoints.startPoint) >= DIAGRAMO.CubicCurve.REFRESH_DISTANCE ||
            DIAGRAMO.Util.distance(this.endPoint, figurePoints.endPoint) >= DIAGRAMO.CubicCurve.REFRESH_DISTANCE ||
            DIAGRAMO.Util.distance(this.controlPoint2, figurePoints.controlPoint2) >= DIAGRAMO.CubicCurve.REFRESH_DISTANCE ||
            DIAGRAMO.Util.distance(this.controlPoint1, figurePoints.controlPoint1) >= DIAGRAMO.CubicCurve.REFRESH_DISTANCE ||
            DIAGRAMO.Util.distance(this.startPoint, points[0]) >= DIAGRAMO.QuadCurve.REFRESH_DISTANCE ||
            DIAGRAMO.Util.distance(this.endPoint, points[points.length - 1]) >= DIAGRAMO.QuadCurve.REFRESH_DISTANCE;
    },

    /**Return the {Point} corresponding a t value, from parametric equation of Curve
     * @param {Number} t the value of parameter t, where t in [0,1]*/
    getPoint: function(t){
        var a = Math.pow((1 - t), 3);
        var b = 3 * t * Math.pow((1 - t), 2);
        var c = 3 * Math.pow(t, 2) * (1 - t);
        var d = Math.pow(t, 3);
        var Xp = a * this.startPoint.x + b * this.controlPoint1.x + c * this.controlPoint2.x + d * this.endPoint.x;
        var Yp = a * this.startPoint.y + b * this.controlPoint1.y + c * this.controlPoint2.y + d * this.endPoint.y;

        return new DIAGRAMO.Point(Xp, Yp);
    },

    /**Return the {Point} corresponding the t certain t value.
     * NOTE: t is the visual percentage of a point from the start of the
     * primitive and the result is
     * different than the t from getPoint(...) method
     * @param {Number} t the value of parameter t, where t in [0,1]*/
    getVisualPoint:function (t){
        var points = this.getPoints();
        var polyline = new DIAGRAMO.Polyline();
        polyline.points = points;

        return polyline.getVisualPoint(t);
    },



    toString:function(){
        return 'quad(' + this.startPoint + ',' + this.controlPoint1 + ',' + this.controlPoint2 + ',' + this.endPoint + ')';
    },
});
