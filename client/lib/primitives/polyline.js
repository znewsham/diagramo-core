/**
  * Creates an instance of a DIAGRAMO.Polyline
  *
  * @constructor
  * @this {Polyline}
  * @author Alex Gheorghiu <alex@scriptoid.com>
  **/
function Polyline(startPoint, endPoint){
    DIAGRAMO.PointBasedPrimitive.call(this);
    /**An {Array} of {@link DIAGRAMO.Point}s*/
    this.points = [];
    if(startPoint){
      this.points.push(startPoint);
      if(endPoint){
        this.points.push(endPoint);
      }
    }
    /**The {@link DIAGRAMO.Style} of the polyline*/
    this.style = new DIAGRAMO.Style();
    this.style.gradientBounds = this.getBounds();

    /**The starting {@link DIAGRAMO.Point}.
     * Required for path, we could use getPoints(), but this existed first.
     * Also its a lot simpler. Each other element used in path already has a startPoint
     * //TODO: (added by alex) This is bullshit....we need to remove this kind of junky code
     * @deprecated
     **/
    this.startPoint = null;

    /**Serialization type*/
    this.oType = 'Polyline'; //object type used for JSON deserialization
}
DIAGRAMO.Polyline = Polyline;
/**Creates a {Polyline} out of JSON parsed object
 *@param {JSONObject} o - the JSON parsed object
 *@return {Polyline} a newly constructed DIAGRAMO.Polyline
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
Polyline.load = function(o){
    var newPolyline = new DIAGRAMO.Polyline();
    newPolyline.points = DIAGRAMO.Point.loadArray(o.points);
    newPolyline.style = DIAGRAMO.Style.load(o.style);
    newPolyline.startPoint = DIAGRAMO.Point.load(o.points[0]);
    DIAGRAMO.Primitive.load(o, newPolyline);
    return newPolyline;
};

_.extend(Polyline.prototype, DIAGRAMO.PointBasedPrimitive.prototype, {
    toJSONObject: function(){
        var ret = _.extend({}, DIAGRAMO.PointBasedPrimitive.prototype.toJSONObject.call(this),{
            oType: "DIAGRAMO.Polyline",
            points: DIAGRAMO.Point.toJSONArray(this.points)
        });
        return ret;
    },
    addPoint:function(point){
        if(this.points.length==0){
            this.startPoint=point;
        }
        this.points.push(point);

        // update bound coordinates for gradient
        this.style.gradientBounds = this.getBounds();
    },

    transform:function(matrix){
        if(this.style!=null){
            this.style.transform(matrix);
        }
        for(var i=0; i<this.points.length; i++){
            this.points[i].transform(matrix);
        }
    },

    getPoints:function(step){
      if(!step){
        return this.points;
      }
      else{
        var points = [];
        for(var i = 0; i <= 1; i += step){
          var tmp = {step: step};
          points.push(this.getVisualPoint(i, tmp));
          if(tmp.point){
            points.push(tmp.point);
          }
        }
        return points;
      }
    },

    getSegment:function(t){
      var l = this.getLength();


      var walked = 0;
      var i;
      for(i=0; i< this.points.length-2; i++){
          if( walked + DIAGRAMO.Util.distance(this.points[i], this.points[i+1]) > l * t ){
              break;
          }

          walked += DIAGRAMO.Util.distance(this.points[i], this.points[i+1]);
      }

      var rest = l * t - walked;
      return [i, rest];
    },
    /**Return the {Point} corresponding the t certain t value.
     * NOTE: t is the visual percentage of a point from the start of the
     * primitive and the result is
     * different than the t from getPoint(...) method
     * @param {Number} t the value of parameter t, where t in [0,1]*/
    getVisualPoint:function (t, extraHolder){

      var tmp = this.getSegment(t);;
        var i = tmp[0];
        var rest = tmp[1];
        var currentSegmentLength = DIAGRAMO.Util.distance(this.points[i], this.points[i+1]);
        //find the position/ration of the middle of DIAGRAMO.Polyline on current segment
        var segmentPercent = rest / currentSegmentLength;
        var line = new DIAGRAMO.Line(this.points[i], this.points[i+1]);
        var THEpoint = line.getPoint(segmentPercent);

        //if we are trying to get all the points, but will miss the last one, add it
        if(extraHolder && extraHolder.step && (t + extraHolder.step > 1 || i != this.getSegment(t + extraHolder.step)[0])){
          extraHolder.point = line.endPoint;
        }
        return THEpoint;
    },
    getPoint: function(t, extraHolder){
      return this.getVisualPoint(t, extraHolder);
    },
    clone:function(){
        var ret=new DIAGRAMO.Polyline();
        for(var i=0; i<this.points.length; i++){
            ret.addPoint(this.points[i].clone());
        }
        ret.style=this.style.clone();
        return ret;
    },

    /**Return the length of the polyline
     * by summing up all the length of all
     * segments*/
    getLength : function(){
        var l = 0;
        for(var i=0; i< this.points.length-1; i++){
            l += DIAGRAMO.Util.distance(this.points[i], this.points[i+1]);
        }

        return l;
    },


    equals:function(anotherPolyline){
        if(!anotherPolyline instanceof DIAGRAMO.Polyline){
            return false;
        }
        if(anotherPolyline.points.length == this.points.length){
            for(var i=0; i<this.points.length; i++){
                if(!this.points[i].equals(anotherPolyline.points[i])){
                    return false;
                }
            }
        }
        else{
            return false;
        }

        if(!this.style.equals(anotherPolyline.style)){
            return false;
        }

        if(!this.startPoint.equals(anotherPolyline.startPoint)){
            return false;
        }



        return true;
    },


    paint:function(context){
        if(this.trigger("beforePaint")){
          return;
        }
        context.save();
        if(this.style != null){
          this.style.setupContext(context);
        }
        context.beginPath();
        context.moveTo(this.points[0].x, this.points[0].y);
        for(var i=1; i<this.points.length; i++){
            context.lineTo(this.points[i].x, this.points[i].y);
            //Log.info("Polyline:paint()" + " Paint a line to [" + this.points[i].x + ',' + this.points[i].y  + ']');
        }


        if(this.style.fillStyle!=null && this.style.fillStyle!=""){
            context.fill();
            //Log.info("Polyline:paint() We have fill: " + this.style.fillStyle)
        }

        if(this.style.strokeStyle !=null && this.style.strokeStyle != ""){
            //Log.info("Polyline:paint() We have stroke: " + this.style.strokeStyle)
            context.strokeStyle = this.style.strokeStyle;
            context.stroke();
        }
        context.restore();
    },


    contains:function(x, y){
        return DIAGRAMO.Util.isPointInside(new DIAGRAMO.Point(x, y), this.getPoints())
    },


    near:function(x, y, radius){
        for(var i=0; i< this.points.length-1; i++){
            var l = new DIAGRAMO.Line(this.points[i], this.points[i+1]);

            if(l.near(x,y,radius)){
                return true;
            }
        }

        return false;
    },
});
