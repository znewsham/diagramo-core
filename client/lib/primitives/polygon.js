/**
  * Creates an instance of a DIAGRAMO.Polygon
  *
  * @constructor
  * @this {Polygon}
  * @author Alex Gheorghiu <alex@scriptoid.com>
  **/
function Polygon(){
    DIAGRAMO.PointBasedPrimitive.call(this);
    /**An {Array} of {@link DIAGRAMO.Point}s*/
    this.points = [];

    /**The {@link DIAGRAMO.Style} of the polygon*/
    this.style = new DIAGRAMO.Style();
    this.style.gradientBounds = this.getBounds();

    /**Serialization type*/
    this.oType = 'Polygon'; //object type used for JSON deserialization
}
DIAGRAMO.Polygon = Polygon;
/**Creates a {Polygon} out of JSON parsed object
 *@param {JSONObject} o - the JSON parsed object
 *@return {Polygon} a newly constructed DIAGRAMO.Polygon
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
Polygon.load = function(o, newPolygon){
    if(!newPolygon){
      newPolygon = new DIAGRAMO.Polygon();
    }
    newPolygon.points = DIAGRAMO.Point.loadArray(o.points);
    newPolygon.style = DIAGRAMO.Style.load(o.style);
    DIAGRAMO.Primitive.load(o, newPolygon);
    return newPolygon;
}


_.extend(Polygon.prototype, DIAGRAMO.PointBasedPrimitive.prototype,{
    toJSONObject: function(){
        var ret = _.extend({}, DIAGRAMO.PointBasedPrimitive.prototype.toJSONObject.call(this),{
            oType: "DIAGRAMO.Polygon",
            points: DIAGRAMO.Point.toJSONArray(this.points)
        });
        return ret;
    },

    addPoint:function(point){
        this.points.push(point);

        // update bound coordinates for gradient
        this.style.gradientBounds = this.getBounds();
    },


    paint:function(context){
        if(this.trigger("beforePaint")){
          return;
        }
        context.save();
        context.beginPath();
        if(this.style!=null){
            this.style.setupContext(context);
        }
        if(this.points.length > 1){
            context.moveTo(this.points[0].x, this.points[0].y);
            for(var i=1; i<this.points.length; i++){
                context.lineTo(this.points[i].x, this.points[i].y)
            }
        }
        context.closePath();

        //fill current path
        if(this.style.fillStyle != null && this.style.fillStyle != ""){
            context.fill();
        }

        //stroke current path
        if(this.style.strokeStyle != null && this.style.strokeStyle != ""){
            context.stroke();
        }
        context.restore();
    },


    getPoints:function(){
        var p = [];
        for (var i=0; i<this.points.length; i++){
            p.push(this.points[i]);
        }
        if(p.length != 0){
          p.push(this.points[0]);//go back to start
        }
        return p;
    },

    near:function(x,y,radius){
        var i=0;
        for(i=0; i< this.points.length-1; i++){
            var l = new DIAGRAMO.Line(this.points[i], this.points[i+1]);
            if(l.near(x,y,radius)){
                return true;
            }
        }
        l=new DIAGRAMO.Line(this.points[i], this.points[0]);
        if(l.near(x,y,radius)){
            return true;
        }
        return false;
    },

    clone:function(){
        var ret=new DIAGRAMO.Polygon();
        for(var i=0; i<this.points.length; i++){
            ret.addPoint(this.points[i].clone());
        }
        ret.style = this.style.clone();

        return ret;
    },

    contains:function(x, y, includeBorders){
        var inPath = false;
        var p = new DIAGRAMO.Point(x,y);
        if(!p){
            alert('Polygon: P is null');
        }

        if (includeBorders) {
            return DIAGRAMO.Util.isPointInsideOrOnBorder(p, this.points);
        } else {
            return DIAGRAMO.Util.isPointInside(p, this.points);
        }
    },

    transform:function(matrix){
        if(this.style!=null){
            this.style.transform(matrix);
        }
        for(var i=0; i < this.points.length; i++){
            this.points[i].transform(matrix);
        }
    },

    toString:function(){
        var result = 'polygon(';
        for(var i=0; i < this.points.length; i++){
            result += this.points[i].toString() + ' ';
        }
        result += ')';
        return result;
    },

    /**Render the SVG fragment for this primitive*/
    toSVG:function(){
        //<polygon points="220,100 300,210 170,250" style="fill:#cccccc; stroke:#000000;stroke-width:1"/>
        var result = "\n" + repeat("\t", INDENTATION) + '<polygon points="';
        for(var i=0; i < this.points.length; i++){
            result += this.points[i].x + ',' + this.points[i].y + ' ';
        }
        result += '" '
        //+  'style="fill:#cccccc;stroke:#000000;stroke-width:1"'
        +  this.style.toSVG()
        +  ' />';
        return result;
    }
});
