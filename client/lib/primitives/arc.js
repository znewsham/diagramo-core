
/**
 * Draws an arc.
 * To allow easy transformations of the arc we will simulate the arc by a series of curves
 *
 * @constructor
 * @this {Arc}
 * @param {Number} x - the X coordinates of the "imaginary" circle of which the arc is part of
 * @param {Number} y - the Y coordinates of the "imaginary" circle of which the arc is part of
 * @param {Number} radius - the radius of the arc
 * @param {Number} startAngle - the degrees (not radians as in Canvas'specs) of the starting angle for this arc
 * @param {Number} endAngle - the degrees (not radians as in Canvas'specs) of the starting angle for this arc
 * @param {Number} direction - the direction of drawing. For now it's from startAngle to endAngle (anticlockwise). Not really used
 * @param {Number} styleFlag (optional) -
 * 1: close path between start of arc and end,
 * 2: draw pie slice, line to center point, line to start point
 * default: empty/0/anything else: just draw the arc
 * TODO: make it a class constant
 * @see <a href="http://STACKoverflow.com/questions/2688808/drawing-quadratic-bezier-circles-with-a-given-radius-how-to-determine-control-po">http://STACKoverflow.com/questions/2688808/drawing-quadratic-bezier-circles-with-a-given-radius-how-to-determine-control-po</a>
 **/

function Arc(x, y, radius, startAngle, endAngle, direction, styleFlag){
    DIAGRAMO.Primitive.call(this);
    /**End angle. Required for dashedArc*/
    this.endAngle = endAngle;

    /**Start angle. required for dashedArc*/
    this.startAngle = startAngle;

    /**The center {@link DIAGRAMO.Point} of the circle*/
    this.middle = new DIAGRAMO.Point(x,y);

    /**The radius of the circle*/
    this.radius = radius;

    /**An {Array} of {@link DIAGRAMO.QuadCurve}s used to draw the arc*/
    this.curves = [];

    /**The start {@link DIAGRAMO.Point}*/
    this.startPoint = null;

    /**The end {@link DIAGRAMO.Point}*/
    this.endPoint = null;

    /**The start angle, in radians*/
    this.startAngleRadians = 0;

    /**The end angle, in radians*/
    this.endAngleRadians = 0;

    this.setupCurves();

    /**The style flag - see  contructor's arguments*/
    this.styleFlag = styleFlag;

    /**The {@link DIAGRAMO.Style} of the arc*/
    this.style = new DIAGRAMO.Style();
    this.style.gradientBounds = this.getBounds();

    /**Adding a reference to the end point makes the transform code hugely cleaner*/
    this.direction = direction;

    /**Object type used for JSON deserialization*/
    this.oType = 'Arc';
}

DIAGRAMO.Arc = Arc;
/**Creates a {Arc} out of JSON parsed object
 *@param {JSONObject} o - the JSON parsed object
 *@return {Arc} a newly constructed DIAGRAMO.Arc
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
Arc.load = function(o){
    var newArc = new DIAGRAMO.Arc();

    newArc.endAngle = o.endAngle;
    newArc.startAngle = o.startAngle;
    newArc.middle = DIAGRAMO.Point.load(o.middle);
    newArc.radius = o.radius
    newArc.curves = DIAGRAMO.QuadCurve.loadArray(o.curves);

    /*we need to load these 'computed' values as they are computed only in constructor :(
     *TODO: maybe make a new function setUp() that deal with this*/
    newArc.startPoint = DIAGRAMO.Point.load(o.startPoint);
    newArc.endPoint = DIAGRAMO.Point.load(o.endPoint);
    newArc.startAngleRadians = o.startAngleRadians;
    newArc.endAngleRadians = o.endAngleRadians;

    newArc.styleFlag = o.styleFlag;
    newArc.style = DIAGRAMO.Style.load(o.style);
    newArc.direction = o.direction;
    DIAGRAMO.Primitive.load(o, newArc);

    return newArc;
}

/**Creates a {Arc} out of an Array of JSON parsed object
 *@param {Array} v - an {Array} of JSON parsed objects
 *@return {Array}of newly constructed DIAGRAMO.Arcs
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
Arc.loadArray = function(v){
    var newArcs = [];

    for(var i=0; i<v.length; i++){
        newArcs.push(DIAGRAMO.Arc.load(v[i]));
    }

    return newArcs;
}



_.extend(Arc.prototype, DIAGRAMO.Primitive.prototype,{
    setupCurves: function(){
      var x = this.middle.x;
      var y = this.middle.y;
      /**Accuracy. It tells the story of how many DIAGRAMO.QuadCurves we will use*/
      var numControlPoints = 8;
      this.curves = [];
      //code shamelessly stollen from the above site.
      var start = Math.PI/180 * this.startAngle; //convert the angles back to radians
      this.startAngleRadians = start;
      this.endAngleRadians = Math.PI/180 * this.endAngle;
      var arcLength = (Math.PI/180*(this.endAngle-this.startAngle))/ numControlPoints;
      for (var i = 0; i < numControlPoints; i++) {
          if (i < 1)
          {
              this.startPoint = new DIAGRAMO.Point(x + this.radius * Math.cos(arcLength * i),y + this.radius * Math.sin(arcLength * i))
          }
          var startPoint=new DIAGRAMO.Point(x + this.radius * Math.cos(arcLength * i),y + this.radius * Math.sin(arcLength * i))

          //control this.radius formula
          //where does it come from, why does it work?
          var controlRadius = this.radius / Math.cos(arcLength * .5);

          //the control point is plotted halfway between the arcLength and uses the control this.radius
          var controlPoint=new DIAGRAMO.Point(x + controlRadius * Math.cos(arcLength * (i + 1) - arcLength * .5),y + controlRadius * Math.sin(arcLength * (i + 1) - arcLength * .5))
          if (i == (numControlPoints - 1))
          {
              this.endPoint = new DIAGRAMO.Point(x + this.radius * Math.cos(arcLength * (i + 1)),y + this.radius * Math.sin(arcLength * (i + 1)));
          }
          var endPoint=new DIAGRAMO.Point(x + this.radius * Math.cos(arcLength * (i + 1)),y + this.radius * Math.sin(arcLength * (i + 1)));


          //if we arent starting at 0, rotate it to where it needs to be

          //move to origin (O)
          startPoint.transform(DIAGRAMO.Matrix.translationMatrix(-x,-y));
          controlPoint.transform(DIAGRAMO.Matrix.translationMatrix(-x,-y));
          endPoint.transform(DIAGRAMO.Matrix.translationMatrix(-x,-y));

          //rotate by angle (start)
          startPoint.transform(DIAGRAMO.Matrix.rotationMatrix(start));
          controlPoint.transform(DIAGRAMO.Matrix.rotationMatrix(start));
          endPoint.transform(DIAGRAMO.Matrix.rotationMatrix(start));

          //move it back to where it was
          startPoint.transform(DIAGRAMO.Matrix.translationMatrix(x,y));
          controlPoint.transform(DIAGRAMO.Matrix.translationMatrix(x,y));
          endPoint.transform(DIAGRAMO.Matrix.translationMatrix(x,y));

          this.curves.push(new DIAGRAMO.QuadCurve(startPoint,controlPoint,endPoint));
      }
    },
    toJSONObject: function(){
        var ret = _.extend({}, DIAGRAMO.Primitive.prototype.toJSONObject.call(this),{
            oType: "DIAGRAMO.Arc",
            startPoint: this.startPoint,
            endPoint: this.endPoint,
            startAngle: this.startAngle,
            endAngle: this.endAngle,
            startAngleRadians: this.startAngleRadians,
            endAngleRadians: this.endAngleRadians,
            middle: this.middle.toJSONObject(),
            radius: this.radius,
            direction: this.direction,
            styleFlag: this.styleFlag,
            curves: []
        });
        this.curves.forEach(function(curve){
            ret.curves.push(curve.toJSONObject());
        });
        return ret;
    },
    transform:function(matrix){
        /* rotations - ok
         * translations - ok
         * scale - ok
         * skew - NOT ok (i do not know how to preserve points, angles...etc- maybe a Cola :)
         **/

        //transform the style
        if(this.style != null){
            this.style.transform(matrix);
        }

        //transform the center of the circle
        this.middle.transform(matrix);

        //transform each curve
        for(var i=0; i<this.curves.length; i++){
            this.curves[i].transform(matrix);
        }
    },


    paint:function(context){
        if(this.trigger("beforePaint")){
          return;
        }
        context.save();
        context.beginPath();

        if(this.style!=null){
            this.style.setupContext(context);
        }
        context.lineWidth = this.style.lineWidth;
        //context.arc(x,y,radius,(Math.PI/180)*startAngle,(Math.PI/180)*endAngle,direction);
        context.moveTo(this.curves[0].startPoint.x, this.curves[0].startPoint.y);
        for(var i=0; i<this.curves.length; i++){
            context.quadraticCurveTo(this.curves[i].controlPoint.x, this.curves[i].controlPoint.y
                ,this.curves[i].endPoint.x, this.curves[i].endPoint.y);
        //curves[i].paint(context);
        }

        if(this.styleFlag == 1){
            context.closePath();
        }
        else if(this.styleFlag == 2){
            context.lineTo(this.middle.x, this.middle.y);
            context.closePath();
        }

        //first fill
        if(this.style.fillStyle!=null && this.style.fillStyle!=""){
            context.fill();
        }

        //then stroke
        if(this.style.strokeStyle!=null && this.style.strokeStyle!=""){
            context.stroke();
        }
        context.restore();

    },

    clone:function(){
        var ret = new DIAGRAMO.Arc(this.middle.x, this.middle.y, this.radius, this.startAngle, this.endAngle, this.direction, this.styleFlag);
        for (var i=0; i< this.curves.length; i++){
            ret.curves[i]=this.curves[i].clone();
        }
        ret.style=this.style.clone();
        return ret;
    },

    equals:function(anotherArc){
        if(!anotherArc instanceof DIAGRAMO.Arc){
            return false;
        }

        //check curves
        for(var i = 0 ; i < this.curves.lenght; i++){
            if(!this.curves[i].equals(anotherArc.curves[i])){
                return false;
            }
        }

        return this.startAngle == anotherArc.startAngle
        && this.endAngle == anotherArc.endAngle
        && this.middle.equals(anotherArc.middle)
        && this.radius == anotherArc.radius
        && this.numControlPoints == anotherArc.numControlPoints
        && this.startPoint.equals(anotherArc.startPoint)
        && this.endPoint.equals(anotherArc.endPoint)
        && this.startAngleRadians == anotherArc.startAngleRadians
        && this.endAngleRadians == anotherArc.endAngleRadians
    ;
    },

    near:function(thex,they,theradius){
        for(var i=0; i<this.curves.length; i++){
            if(this.curves[i].near(thex,they,theradius)){
                return true;
            }
        }
        //return (distance && angle) || finishLine || startLine || new DIAGRAMO.Point(x,y).near(thex,they,theradius);

        return false;
    },

    contains: function(thex,they){
        var p = this.getPoints();
        return DIAGRAMO.Util.isPointInside((new DIAGRAMO.Point(thex,they)), p);

    },

    /**Get the points of the DIAGRAMO.Arc
     *@return {Array} of {Point}s
     *@author Zack
     **/
    getPoints:function(step){
        var p = [];
        if(this.styleFlag ==2){
            p.push(this.middle);
        }
        else if(!step){
          this.curves.forEach(function(c){
            p = p.concat(c.getPoints());
          });
          return p;
        }
        var portions = [

        ];
        var totalLength = 0;
        this.curves.forEach(function(c){
          totalLength += c.getLength();
        });
        this.curves.forEach(function(c){
          portions.push(1 / totalLength * c.getLength());
        });
        var targetLength = totalLength / 1 * step;

        for(i = 0; i < 1; i+=step){
          var walked = 0;
          var seg = 0;
          for(a = 0; a < portions.length; a++){
            if(walked + portions[a] > i){
              seg = portions[a];
              break;
            }
            walked += portions[a];
          }
          if(a >= this.curves.length){
            p.push(this.curves[this.curves.length - 1].getPoint(1));
          }
          else{
            p.push(this.curves[a].getPoint((i - walked) / seg));
          }
        }
        if(this.startAngle == 0 && this.endAngle == 360){
          p.push(this.curves[0].getPoint(0));
        }
        return p;
    },

    getBounds:function(){
        return DIAGRAMO.Util.getBounds(this.getPoints());
    },

    toString:function(){
        return 'arc(' + new DIAGRAMO.Point(this.x,this.y) + ','  + this.radius + ',' + this.startAngle + ',' + this.endAngle + ',' + this.direction + ')';
    },
});
