/**
 * A path contains a number of elements (like shape) but they are drawn as one, i.e.
 *
 * @example
 * begin path
 *  loop to draw shapes
 *      draw shape
 *  close loop
 * close path
 *
 *
 * @constructor
 * @this {Path}
 **/
function Path() {
    DIAGRAMO.Primitive.call(this);

    /**The {@link DIAGRAMO.Style} used for drawing*/
    this.style = new DIAGRAMO.Style();
    this.style.gradientBounds = this.getBounds();

    /**Object type used for JSON deserialization*/
    this.oType = 'Path';
}
DIAGRAMO.Path = Path;
/**Creates a new {Path} out of JSON parsed object
 *@param {JSONObject} o - the JSON parsed object
 *@return {Path} a newly constructed {Path}
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
Path.load = function(o){
    var newPath = new Path(); //fake path

    newPath.style = DIAGRAMO.Style.load(o.style);

    for(var i=0; i< o.paintables.length; i++){
        newPath.paintables.push(DIAGRAMO.load(o.paintables[i]));

    }

    DIAGRAMO.Primitive.load(o, newPath);
    return newPath;
}


_.extend(Path.prototype, DIAGRAMO.Primitive.prototype,{
    toJSONObject: function(){
        var ret = _.extend({}, DIAGRAMO.Primitive.prototype.toJSONObject.call(this),{
            oType: "DIAGRAMO.Path"
        });
        return ret;
    },

    transform:function(matrix){
        for(var i = 0; i<this.paintables.length; i++ ){
            this.paintables[i].transform(matrix);
        }
    },

    addPrimitive:function(primitive){
        this.paintables.push(primitive);

        // update bound coordinates for gradient
        this.style.gradientBounds = this.getBounds();
    },

    contains: function(x,y){
        var points = [];
        for(var i=0; i<this.paintables.length; i++){
            if(this.paintables[i].contains(x,y)){
                return true;
            }
            var curPoints = this.paintables[i].getPoints();
            for(var a=0; a<curPoints.length; a++){
                points.push(curPoints[a]);
            }
        }
        return DIAGRAMO.Util.isPointInside(new DIAGRAMO.Point(x,y),points);
    },

    near: function(x,y,radius){
        var points = [];
        for(var i=0; i<this.paintables.length; i++){
            if(this.paintables[i].near(x,y,radius)){
                return true;
            }
        }
        return false;
    },

    getPoints:function(step){
        var points = [];
        for (var i=0; i<this.paintables.length; i++){
            points = points.concat(this.paintables[i].getPoints(step));
        }
        return points;
    },
    getBounds:function(){
        var points = [];
        for (var i in this.paintables) {
            var bounds = this.paintables[i].getBounds();
            points.push(new DIAGRAMO.Point(bounds[0], bounds[1]));
            points.push(new DIAGRAMO.Point(bounds[2], bounds[3]));
        }
        return DIAGRAMO.Util.getBounds(points);
    },

    clone:function(){
        var ret = new Path();
        for (var i=0; i<this.paintables.length; i++){
            var p = this.paintables[i].clone();
            p.style = this.paintables[i].style.clone();
            ret.addPrimitive(p);
            if(this.paintables[i].parentFigure){
                ret.paintables[i].parentFigure=ret;
            }
        }
        ret.style=this.style.clone();
        return ret;
    },

    /**@author: Alex Gheorghiu <alex@scriptoid.com>*/
    equals : function(anotherPath){
        if(!anotherPath instanceof Path){
            return false;
        }

        for(var i=0; i<this.paintables.length; i++){
            if(!this.paintables[i].equals(anotherPath.paintables[i])){
                return  false;
            }
        }
        return true;
    },

    paint:function(context){
        if(this.trigger("beforePaint")){
          return;
        }
        context.save();

        if(this.style != null){
            this.style.setupContext(context);
        }

        //PAINT FILL
        //this loop is the one for the fill, we keep the reference.
        //if you try to put these two together, you will get a line that is the same colour,
        //even if you define different colours for each part of the line (i.e. fig 19)
        //not allowing multiple colours in a single path will clean this code up hugely.
        //
        if(this.style.fillStyle != null && this.style.fillStyle != "" ){
            context.beginPath();
            var firstPoint = this.paintables[0].getPoints()[0]; //find first point of primitive
            context.moveTo(firstPoint.x, firstPoint.y);
            for(var i = 0; i<this.paintables.length; i++ ){
                var primitive  = this.paintables[i];
                if(primitive instanceof DIAGRAMO.Line){
                    context.lineTo(primitive.endPoint.x,primitive.endPoint.y);
                }
                else if(primitive instanceof DIAGRAMO.Polyline){
                    for(var a=0; a<primitive.points.length; a++){
                        context.lineTo(primitive.points[a].x,primitive.points[a].y);
                    }
                }
                else if(primitive instanceof DIAGRAMO.Polygon){
                    for(var a=0; a<primitive.points.length; a++){
                        context.lineTo(primitive.points[a].x,primitive.points[a].y);
                    }
                }
                else if(primitive instanceof DIAGRAMO.QuadCurve){
                    context.quadraticCurveTo(primitive.controlPoint.x, primitive.controlPoint.y,
                        primitive.endPoint.x, primitive.endPoint.y);
                }
                else if(primitive instanceof DIAGRAMO.CubicCurve){
                    context.bezierCurveTo(primitive.controlPoint1.x, primitive.controlPoint1.y,
                        primitive.controlPoint2.x, primitive.controlPoint2.y, primitive.endPoint.x, primitive.endPoint.y)
                }
            }
            context.fill();
        }

        //PAINT STROKE
        //This loop draws the lines of each individual shape. Each part might have a different strokeStyle !
        if(this.style.strokeStyle != null && this.style.strokeStyle != "" ){
            for(var i = 0; i<this.paintables.length; i++ ){
                var primitive  = this.paintables[i];
                var firstPoint = primitive.getPoints()[0]; //find first point of primitive

                context.save();
                context.beginPath();

                //TODO: what if a primitive does not have a start point?
                context.moveTo(firstPoint.x, firstPoint.y);


                if(primitive.paintDelegate != null){
                  primitive.paintDelegate.style = primitive.style;
                  context.save();
                  primitive.paintDelegate.paint(context);
                  context.restore();
                }
                else if(primitive instanceof DIAGRAMO.Line){
                    context.lineTo(primitive.endPoint.x,primitive.endPoint.y);
                    //context.closePath(); // added for line's correct Chrome's displaying
                    //Log.info("line");
                }
                else if(primitive instanceof DIAGRAMO.Polyline){
                    for(var a=0; a<primitive.points.length; a++){
                        context.lineTo(primitive.points[a].x,primitive.points[a].y);
                    }
                }
                else if(primitive instanceof DIAGRAMO.Polygon){
                    for(var a=0; a<primitive.points.length; a++){
                        context.lineTo(primitive.points[a].x,primitive.points[a].y);
                    }
                }
                else if(primitive instanceof DIAGRAMO.QuadCurve){
                    context.quadraticCurveTo(primitive.controlPoint.x, primitive.controlPoint.y, primitive.endPoint.x, primitive.endPoint.y);
                //Log.info("quadcurve");
                }
                else if(primitive instanceof DIAGRAMO.CubicCurve){
                    context.bezierCurveTo(primitive.controlPoint1.x, primitive.controlPoint1.y, primitive.controlPoint2.x, primitive.controlPoint2.y, primitive.endPoint.x, primitive.endPoint.y)
                //Log.info("cubiccurve");
                }
                else if(primitive instanceof DIAGRAMO.Arc){
                    context.arc(primitive.startPoint.x, primitive.startPoint.y, primitive.radius, primitive.startAngleRadians, primitive.endAngleRadians, true)
                //Log.info("arc" + primitive.startPoint.x + " " + primitive.startPoint.y);
                }
                else
                {
                //Log.info("unknown primitive");
                }

                //save primitive's old style
                var oldStyle = primitive.style.clone();

                //update primitive's style
                if(primitive.style == null){
                    primitive.style = this.style;
                }
                else{
                    primitive.style.merge(this.style);
                }

                //use primitive's style
                primitive.style.setupContext(context);

                //stroke it
                context.stroke();

                //change primitive' style back to original one
                primitive.style = oldStyle;

                context.restore();
            }
        }

        context.restore();

    },

});
