/**
  * Creates an instance of a quad curve.
  * A curved line determined by 2 normal points (startPoint and endPoint) and 1 control point (controlPoint)
  *
  * @constructor
  * @this {QuadCurve}
  * @param {Point} startPoint - starting point of the line
  * @param {Point} controlPoint - the control point of the line
  * @param {Point} endPoint - the ending point of the line
  * @see <a href="http://en.wikipedia.org/wiki/B%C3%A9zier_curve#Quadratic_B.C3.A9zier_curves">http://en.wikipedia.org/wiki/B%C3%A9zier_curve#Quadratic_B.C3.A9zier_curves</a>
 **/
function QuadCurve(startPoint, controlPoint, endPoint){
    DIAGRAMO.Curve.call(this);
    this._cachedPoints = {};
    /**The start {@link DIAGRAMO.Point}*/
    this.startPoint = startPoint;

    /**The controll {@link DIAGRAMO.Point}*/
    this.controlPoint = controlPoint;

    /**The end {@link DIAGRAMO.Point}*/
    this.endPoint = endPoint;

    /**The {@link DIAGRAMO.Style} of the quad*/
    this.style = new DIAGRAMO.Style();
    this.style.gradientBounds = this.getBounds();

    /**Serialization type*/
    this.oType = 'QuadCurve'; //object type used for JSON deserialization
}


DIAGRAMO.QuadCurve = QuadCurve;
/**Creates a {QuadCurve} out of JSON parsed object
 *@param {JSONObject} o - the JSON parsed object
 *@return {QuadCurve} a newly constructed DIAGRAMO.QuadCurve
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
QuadCurve.load = function(o){
    var newQuad = new DIAGRAMO.QuadCurve(
        DIAGRAMO.Point.load(o.startPoint),
        DIAGRAMO.Point.load(o.controlPoint),
        DIAGRAMO.Point.load(o.endPoint)
    );

    newQuad.style = DIAGRAMO.Style.load(o.style);
    DIAGRAMO.Primitive.load(o, newQuad);
    return newQuad;
};

/**Creates an {Array} of {QuadCurve} out of JSON parsed object
 *@param {JSONObject} v - the JSON parsed object (actually an {Array} of {JSONObject}s
 *@return {Array} of {QuadCurve}s
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
QuadCurve.loadArray = function(v){
    var quads = [];

    for(var i=0; i<v.length; i++){
        quads.push(DIAGRAMO.QuadCurve.load(v[i]));
    }

    return quads;
};

QuadCurve.REFRESH_DISTANCE = 1.0;

_.extend(QuadCurve.prototype, DIAGRAMO.Curve.prototype,{
    toJSONObject: function(){
        return {
            style: this.style,
            id: this.id,
            oType: "DIAGRAMO.QuadCurve",
            startPoint: this.startPoint.toJSONObject(),
            controlPoint: this.controlPoint.toJSONObject(),
            endPoint: this.endPoint.toJSONObject()
        };
    },
    transform:function(matrix){
        if(this.style!=null){
            this.style.transform(matrix);
        }
        this.startPoint.transform(matrix);
        this.controlPoint.transform(matrix);
        this.endPoint.transform(matrix);
    },

    refreshPoints: function(figurePoints, points){
       return DIAGRAMO.Util.distance(this.startPoint, figurePoints.startPoint) >= DIAGRAMO.QuadCurve.REFRESH_DISTANCE ||
           DIAGRAMO.Util.distance(this.endPoint, figurePoints.endPoint) >= DIAGRAMO.QuadCurve.REFRESH_DISTANCE ||
           DIAGRAMO.Util.distance(this.controlPoint, figurePoints.controlPoint) >= DIAGRAMO.QuadCurve.REFRESH_DISTANCE ||
           DIAGRAMO.Util.distance(this.startPoint, points[0]) >= DIAGRAMO.QuadCurve.REFRESH_DISTANCE ||
           DIAGRAMO.Util.distance(this.endPoint, points[points.length - 1]) >= DIAGRAMO.QuadCurve.REFRESH_DISTANCE;
    },

    /**
     * Return the point corresponding to parameter value t
     * @param {Number} t the value of t parameter, t in [0,1]
     * @return {Point} the value of t parameter, t in [0,1]
     * @see http://html5tutorial.com/how-to-join-two-bezier-curves-with-the-canvas-api/
     * */
    getPoint:function(t){
        var a = Math.pow((1 - t), 2);
        var b = 2 * (1 - t) * t;
        var c = Math.pow(t, 2);
        var Xp = a * this.startPoint.x + b * this.controlPoint.x + c * this.endPoint.x;
        var Yp = a * this.startPoint.y + b * this.controlPoint.y + c * this.endPoint.y;

        return new DIAGRAMO.Point(Xp, Yp);
    },

    /**Return the {Point} corresponding the t certain t value.
     * NOTE: t is the visual percentage of a point from the start of the
     * primitive and the result is
     * different than the t from getPoint(...) method
     * @param {Number} t the value of parameter t, where t in [0,1]*/
    getVisualPoint:function (t){
        var points = this.getPoints();
        var polyline = new DIAGRAMO.Polyline();
        polyline.points = points;

        return polyline.getVisualPoint(t);
    },



    paint:function(context){
        if(this.trigger("beforePaint")){
          return;
        }
        context.save();
        context.beginPath();

        if(this.style!=null){
            this.style.setupContext(context);
        }

        context.moveTo(this.startPoint.x, this.startPoint.y);
        context.quadraticCurveTo(this.controlPoint.x, this.controlPoint.y, this.endPoint.x, this.endPoint.y);

        //first fill
        if(this.style != null && this.style.fillStyle!=null && this.style.fillStyle!=""){
            context.fill();
        }

        //then stroke
        if(this.style == null || (this.style.strokeStyle!=null && this.style.strokeStyle!="")){
            context.stroke();
        }


        if(false){ //structure polyline
            var polyline = new DIAGRAMO.Polyline();
            polyline.style.strokeStyle = '#ccc';
            polyline.points = this.getPoints();
            polyline.paint(context);
            context.stroke();

            context.fillStyle = "#F00";
            context.fillRect(this.startPoint.x-2, this.startPoint.y-2, 4, 4);
            context.fillRect(this.controlPoint.x-2, this.controlPoint.y-2, 4, 4);
            context.fillRect(this.endPoint.x-2, this.endPoint.y-2, 4, 4);
        }
        context.restore();
    },

    clone:function(){
        var ret=new DIAGRAMO.QuadCurve(this.startPoint.clone(),this.controlPoint.clone(),this.endPoint.clone());
        ret.style=this.style.clone();
        return ret;
    },

    equals:function(anotherQuadCurve){
        if(!anotherQuadCurve instanceof DIAGRAMO.QuadCurve){
            return false;
        }

        return this.startPoint.equals(anotherQuadCurve.startPoint)
        && this.controlPoint.equals(anotherQuadCurve.controlPoint)
        && this.endPoint.equals(anotherQuadCurve.endPoint)
        && this.style.equals(anotherQuadCurve.style);
    },

    toString:function(){
        return 'quad(' + this.startPoint + ',' + this.controlPoint + ',' + this.endPoint + ')';
    },
});
