function Curve(){
    DIAGRAMO.PointBasedPrimitive.call(this);
}

DIAGRAMO.Curve = Curve;
_.extend(Curve.prototype, DIAGRAMO.PointBasedPrimitive.prototype, {


    //TODO: dynamically adjust until the length of a segment is small enough
    //(1 unit)?
    getPoints:function(STEP){
      if(STEP === null){
        var points = [this.startPoint];
        if(this.controlPoint){
          points.push(this.controlPoint);
        }
        else{
          points.push(this.controlPoint1);
          points.push(this.controlPoint2);
        }
        points.push(this.endPoint);
        return points;
      }
        var STEP = STEP == undefined ? 0.01 : STEP;
        if(this._cachedPoints[STEP] != undefined && !this.refreshPoints(this._cachedPoints[STEP].figurePoints, this._cachedPoints[STEP].points)){
            return this._cachedPoints[STEP].points;
        }
        var points = [];
        //+ 1/10 due to floating point error
        for(var t = 0; t <= (1 + (STEP / 10)); t += STEP){
            points.push(this.getPoint(t));
        }
        this._cachedPoints[STEP] = {
            points: points,
            figurePoints: {
                startPoint: {x: this.startPoint.x, y: this.startPoint.y},
                endPoint: {x: this.endPoint.x, y: this.endPoint.y},
                controlPoint: this.controlPoint ? {x: this.controlPoint.x, y: this.controlPoint.y} : undefined,
                controlPoint1: this.controlPoint1 ? {x: this.controlPoint1.x, y: this.controlPoint1.y} : undefined,
                controlPoint2: this.controlPoint2 ? {x: this.controlPoint2.x, y: this.controlPoint2.y} : undefined
            }
        }


        return points;
    },

    contains:function(x,y) {
        var points = this.getPoints();
        var polyline = new DIAGRAMO.Polyline();
        polyline.points = [];
        points.forEach(function(p){
          polyline.points.push(p);
        })
        points.reverse().forEach(function(p){
          polyline.points.push(p);
        })
        return polyline.contains(x, y);
    },


    /**
     *Tests to see if a point is close enough to a cubic curve
     *@param {Number} x - the x coordinates of the point
     *@param {Number} y - the x coordinates of the point
     *@param {Number} radius - the radius of vicinity
     *@author alex
     *@see <a href="http://rosettacode.org/wiki/Bitmap/B%C3%A9zier_curves/Cubic">http://rosettacode.org/wiki/Bitmap/B%C3%A9zier_curves/Cubic</a>
     */
    near:function(x, y, radius){
        var points = this.getPoints();
        var polyline = new DIAGRAMO.Polyline();
        polyline.points = [];
        points.forEach(function(p){
          polyline.points.push(p);
        })
        points.reverse().forEach(function(p){
          polyline.points.push(p);
        })

       return polyline.near(x, y, radius);
    },

    /**Computes the length of the Cubic Curve
     * TODO: This is by far not the best algorithm but only an aproximation.
     * */
    getLength:function(){
        /*Algorithm: split the Bezier curve into an aproximative
       polyline and use polyline's near method*/
        var poly = new DIAGRAMO.Polyline();

        poly.points = this.getPoints();

       return poly.getLength();
    },
});
