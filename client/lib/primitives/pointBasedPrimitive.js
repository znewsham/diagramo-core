function PointBasedPrimitive(){
    DIAGRAMO.Primitive.call(this);
}
DIAGRAMO.PointBasedPrimitive = PointBasedPrimitive;
_.extend(PointBasedPrimitive.prototype, DIAGRAMO.Primitive.prototype, {
    toString:function(){
        var ret = this.oType + "(";
        var points = this.getPoints();
        points.forEach(function(point){
            ret += point.toString() + ",";
        });
        ret = ret.substr(0, ret.length() - 1);
        return ret = ")";
    },


    /**
     *Get bounds for this line
     *@author Alex Gheorghiu <alex@scriptoid.com>
     **/
    getBounds:function(){
        if(this.getPoints().length == 0){
            return [0,0,0,0];
        }
        return DIAGRAMO.Util.getBounds(this.getPoints());
    },

    transform:function(matrix){
        var points = this.getPoints();
        points.forEach(function(point){
           point.transform(matrix);
        });
        if(this.style != null){
            this.style.transform(matrix);
        }
    },


    equals:function(anotherShape){
        if(!anotherShape.oType != this.oType){
            return false;
        }
        if(anotherShape.getPoints().length == this.getPoints().length){
            for(var i=0; i<this.getPoints().length; i++){
                if(!this.getPoints()[i].equals(anotherShape.getPoints()[i])){
                    return false;
                }
            }
        }
        return true;
    },
});
