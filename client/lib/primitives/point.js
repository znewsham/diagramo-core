/**
  * Creates an instance of DIAGRAMO.Point
  *
  *
  * @constructor
  * @this {Point}
  * @param {Number} x The x coordinate of point.
  * @param {Number} y The y coordinate of point.
  * @author Alex Gheorghiu <alex@scriptoid.com>
  * Note: Even if it is named DIAGRAMO.Point this class should be named Dot as Dot is closer
  * then DIAGRAMO.Point from math perspective.
  **/
function Point(x, y){
    /**The x coordinate of point*/
    this.x = x;

    /**The y coordinate of point*/
    this.y = y;

    /**The {@link DIAGRAMO.Style} of the DIAGRAMO.Point*/
    this.style = new DIAGRAMO.Style();

    /**Serialization type*/
    this.oType = 'Point'; //object type used for JSON deserialization
}
DIAGRAMO.Point = Point;

/**Creates a {Point} out of JSON parsed object
 *@param {JSONObject} o - the JSON parsed object
 *@return {Point} a newly constructed DIAGRAMO.Point
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
Point.load = function(o){
    var newPoint = new DIAGRAMO.Point(Number(o.x), Number(o.y));
    newPoint.style = o.style ? DIAGRAMO.Style.load(o.style) : new DIAGRAMO.Style();
    return newPoint;
}


/**Creates an array of points from an array of {JSONObject}s
 *@param {Array} v - the array of JSONObjects
 *@return an {Array} of {Point}s
 **/
Point.loadArray = function(v){
    var newPoints = [];
    for(var i=0; i< v.length; i++){
        newPoints.push(DIAGRAMO.Point.load(v[i]));
    }
    return newPoints;
}


/**Clones an array of points
 *@param {Array} v - the array of {Point}s
 *@return an {Array} of {Point}s
 **/
Point.cloneArray = function(v){
    var newPoints = [];
    for(var i=0; i< v.length; i++){
        newPoints.push(v[i].clone());
    }
    return newPoints;
}

Point.toJSONArray = function(points){
    var ret = [];
    points.forEach(function(point){
        ret.push(point.toJSONObject());
    });
    return ret;
}
_.extend(Point.prototype, DIAGRAMO.Primitive.prototype, {
    toJSONObject: function(){
        return {
            //style: this.style,
            id: this.id,
            oType: "DIAGRAMO.Point",
            x: this.x,
            y: this.y
        };
    },

    /*
     *Transform a point by a tranformation matrix.
     *It is done by multiplication
     *Pay attention on the order of multiplication: The tranformation {Matrix} is
     *multiplied with the {Point} matrix.
     * P' = M x P
     *@param matrix is a 3x3 matrix
     *@see <a href="http://en.wikipedia.org/wiki/Transformation_matrix#Affine_transformations">http://en.wikipedia.org/wiki/Transformation_matrix#Affine_transformations</a>
     **/
    transform:function(matrix){
        if(this.style!=null){
            this.style.transform(matrix);
        }
        var oldX = this.x;
        var oldY = this.y;
        this.x = matrix[0][0] * oldX + matrix[0][1] * oldY + matrix[0][2];
        this.y = matrix[1][0] * oldX + matrix[1][1] * oldY + matrix[1][2];
    },

    /**Paint current {Point} withing a context
     *If you want to use a different style then the default one change the style
     **/
    paint:function(context){
        if(this.trigger("beforePaint")){
          return;
        }
        context.save();
        if(this.style != null){
            this.style.setupContext(context);
        }
        if(this.style.strokeStyle != ""){
            context.fillStyle = this.style.strokeStyle;
            context.beginPath();
            var width = 1;
            if(this.style.lineWidth != null){
                width = parseInt(this.style.lineWidth);
            }
            context.arc(this.x, this.y, width, 0,Math.PI/180*360,false);
            context.fill();
        }
        context.restore();
    },


    /**Tests if this point is similar to other point
     *@param {Point} anotherPoint - the other point
     **/
    equals:function(anotherPoint){
        if(! (anotherPoint instanceof DIAGRAMO.Point) ){
            return false;
        }
        return (this.x == anotherPoint.x)
        && (this.y == anotherPoint.y)
        && this.style.equals(anotherPoint.style);
    },

    /**Clone current DIAGRAMO.Point
     **/
    clone: function(){
        var newPoint = new DIAGRAMO.Point(this.x, this.y);
        newPoint.style = this.style.clone();
        return newPoint;
    },

    /**Tests to see if a point (x, y) is within a range of current DIAGRAMO.Point
     *@param {Numeric} x - the x coordinate of tested point
     *@param {Numeric} y - the x coordinate of tested point
     *@param {Numeric} radius - the radius of the vicinity
     *@author Alex Gheorghiu <alex@scriptoid.com>
     **/
    near:function(x, y, radius){
        var distance = Math.sqrt(Math.pow(this.x - x, 2) + Math.pow(this.y - y, 2));

        return (distance <= radius);
    },

    contains: function(x,y){
        return this.x == x && this.y == y;
    },

    toString:function(){
        return 'point(' + this.x + ',' + this.y + ')';
    },

    getPoints:function(){
        return [this];
    },
});
