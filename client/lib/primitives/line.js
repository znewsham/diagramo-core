
/**
  * Creates an instance of a DIAGRAMO.Line. A DIAGRAMO.Line is actually a segment and not a pure
  * geometrical DIAGRAMO.Line
  *
  * @constructor
  * @this {Line}
  * @param {Point} startPoint - starting point of the line
  * @param {Point} endPoint - the ending point of the line
  * @author Alex Gheorghiu <alex@scriptoid.com>
  **/
function Line(startPoint, endPoint){
    DIAGRAMO.PointBasedPrimitive.call(this);
    /**Starting {@link DIAGRAMO.Point} of the line*/
    this.startPoint = startPoint;

    /**Ending {@link DIAGRAMO.Point} of the line*/
    this.endPoint = endPoint;

    /**The {@link DIAGRAMO.Style} of the line*/
    this.style = new DIAGRAMO.Style();
    this.style.gradientBounds = this.getBounds();

    /**Serialization type*/
    this.oType = 'Line'; //object type used for JSON deserialization

    //hack for connectors
    this.points = [this.startPoint, this.endPoint];
}

DIAGRAMO.Line = Line;
/**Creates a {Line} out of JSON parsed object
 *@param {JSONObject} o - the JSON parsed object
 *@return {Line} a newly constructed DIAGRAMO.Line
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
Line.load = function(o){
    var newLine = new DIAGRAMO.Line(
        DIAGRAMO.Point.load(o.startPoint),
        DIAGRAMO.Point.load(o.endPoint)
    );

    newLine.style = DIAGRAMO.Style.load(o.style);
    DIAGRAMO.Primitive.load(o, newLine);
    return newLine;
}

_.extend(Line.prototype, DIAGRAMO.PointBasedPrimitive.prototype, {
    toJSONObject: function(){
        var ret = _.extend({}, DIAGRAMO.PointBasedPrimitive.prototype.toJSONObject.call(this),{
            oType: "DIAGRAMO.Line",
            startPoint: this.startPoint.toJSONObject(),
            endPoint: this.endPoint.toJSONObject(),
        });

        return ret;
    },

    paint:function(context){
      return DIAGRAMO.Polyline.prototype.paint.call(this, context);
    },

    clone:function(){
        var ret = new DIAGRAMO.Line(this.startPoint.clone(), this.endPoint.clone());
        ret.style = this.style.clone();
        return ret;
    },

    equals:function(anotherLine){
        if(!anotherLine instanceof DIAGRAMO.Line){
            return false;
        }
        return this.startPoint.equals(anotherLine.startPoint)
        && this.endPoint.equals(anotherLine.endPoint)
        && this.style.equals(anotherLine.style);
    },

    /** Tests to see if a point belongs to this line (not as infinite line but more like a segment)
     * Algorithm: Compute line's equation and see if (x, y) verifies it.
     * @param {Number} x - the X coordinates
     * @param {Number} y - the Y coordinates
     * @author Alex Gheorghiu <alex@scriptoid.com>
     **/
    contains: function(x, y){
        // if the point is inside rectangle bounds of the segment
        if (Math.min(this.startPoint.x, this.endPoint.x) <= x
            && x <= Math.max(this.startPoint.x, this.endPoint.x)
            && Math.min(this.startPoint.y, this.endPoint.y) <= y
            && y <= Math.max(this.startPoint.y, this.endPoint.y)) {

            // check for vertical line
            if (this.startPoint.x == this.endPoint.x) {
                return x == this.startPoint.x;
            } else { // usual (not vertical) line can be represented as y = a * x + b
                var a = (this.endPoint.y - this.startPoint.y) / (this.endPoint.x - this.startPoint.x);
                var b = this.startPoint.y - a * this.startPoint.x;
                return parseFloat(y.toFixed(8)) == parseFloat((a * x + b).toFixed(8));
            }
        } else {
            return false;
        }
    },

    /*
     *See if we are near a {Line} by a certain radius (also includes the extremities into computation)
     *@param {Number} x - the x coordinates
     *@param {Number} y - the y coordinates
     *@param {Number} radius - the radius to search for
     *@see http://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
     *@see "Mathematics for Computer Graphics, 2nd Ed., by John Vice, page 227"
     *@author Zack Newsham <zack_newsham@yahoo.co.uk>
     *@author Arty
     *@author Alex
     **/
    near:function(x,y,radius){

        if(this.endPoint.x === this.startPoint.x){ //Vertical line, so the vicinity area is a rectangle
            return ( (this.startPoint.y-radius<=y && this.endPoint.y+radius>=y)
                    || (this.endPoint.y-radius<=y && this.startPoint.y+radius>=y))
            && x > this.startPoint.x - radius && x < this.startPoint.x + radius ;
        }

        if(this.startPoint.y === this.endPoint.y){ //Horizontal line, so the vicinity area is a rectangle
            return ( (this.startPoint.x - radius<=x && this.endPoint.x+radius>=x)
                    || (this.endPoint.x-radius<=x && this.startPoint.x+radius>=x))
                    && y>this.startPoint.y-radius && y<this.startPoint.y+radius ;
        }


        var startX = Math.min(this.endPoint.x,this.startPoint.x);
        var startY = Math.min(this.endPoint.y,this.startPoint.y);
        var endX = Math.max(this.endPoint.x,this.startPoint.x);
        var endY = Math.max(this.endPoint.y,this.startPoint.y);

        /*We will compute the distance from point to the line
         * by using the algorithm from
         * http://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
         * */

        //First we need to find a,b,c of the line equation ax + by + c = 0
        var a = this.endPoint.y - this.startPoint.y;
        var b = this.startPoint.x - this.endPoint.x;
        var c = -(this.startPoint.x * this.endPoint.y - this.endPoint.x * this.startPoint.y);

        //Secondly we get the distance "Mathematics for Computer Graphics, 2nd Ed., by John Vice, page 227"
        var d = Math.abs( (a*x + b*y + c) / Math.sqrt(Math.pow(a,2) + Math.pow(b,2)) );

        //Thirdly we get coordinates of closest line's point to target point
        //http://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line#Cartesian_coordinates
        var closestX = (b * (b*x - a*y) - a*c) / ( Math.pow(a,2) + Math.pow(b,2) );
        var closestY = (a * (-b*x + a*y) - b*c) / ( Math.pow(a,2) + Math.pow(b,2) );

        var r = ( d <= radius && endX>=closestX && closestX>=startX && endY>=closestY && closestY>=startY ) //the projection of the point falls INSIDE of the segment
            || this.startPoint.near(x,y,radius) || this.endPoint.near(x,y,radius); //the projection of the point falls OUTSIDE of the segment

        return  r;

    },

    /**we need to create a new array each time, or we will affect the actual shape*/
    getPoints:function(step){
      if(!step){
        return [this.startPoint, this.endPoint];
      }
      else{
        var points = [];
        for(var i = 0; i <= 1; i += step){
          points.push(this.getPoint(i));
        }
        return points;
      }
    },

    /**Return the {Point} corresponding the t certain t value
     * @param {Number} t the value of parameter t, where t in [0,1], t is like a percent*/
    getPoint: function(t){
        var Xp = t * (this.endPoint.x - this.startPoint.x) + this.startPoint.x;
        var Yp = t * (this.endPoint.y - this.startPoint.y) + this.startPoint.y;

        return new DIAGRAMO.Point(Xp, Yp);
    },

    /**
     * Returns the middle of the line
     * @return {Point} the middle point
     * */
    getMiddle : function(){
        return DIAGRAMO.Util.getMiddle(this.startPoint, this.endPoint);
    },


    getLength : function(){
        return DIAGRAMO.Util.distance(this.startPoint, this.endPoint);
    },
});
