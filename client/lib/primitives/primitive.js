function Primitive(){
    DIAGRAMO.Paintable.call(this);
    this.isSelectable = true;
    this._disabledHandlers = [];
    this.paintables = [];
    this.clickHandlers = null;
    this.mouseOverHandlers = null;
    this.mouseOutHandlers = null;
    this.mouseDownHandlers = null;
    this.mouseUpHandlers = null;
    this.mouseMoveHandlers = null;
    this.keyDownHandlers = null;
    this.keyUpHandlers = null;
    this.beforeTransformHandlers = null;
    this.afterTransformHandlers = null;
    this.beforePaintHandlers = null;

    this.paintDelegate = null;
}
DIAGRAMO.Primitive = Primitive;
DIAGRAMO.Primitive.load = function(jsonObject, primitive){

  if(!primitive){
    primitive = new DIAGRAMO.Primitive();
    jsonObject.paintables.forEach(function(p){
      primitive.paintables.push(DIAGRAMO.load(p));
    });
  }
  if(jsonObject.paintDelegate){
    jsonObject.paintDelegate.paintables[0].item = primitive;
    primitive.paintDelegate = DIAGRAMO.load(jsonObject.paintDelegate);
  }
  primitive.zIndex = jsonObject.zIndex;
  return primitive;
}
_.extend(Primitive.prototype, DIAGRAMO.Paintable.prototype, {
    getSelectedComponent: function(){
      return null;
    },
    getMiddle: function(){
      var bounds = this.getBounds();
      var middle = new DIAGRAMO.Point(bounds[0] + (bounds[2] - bounds[0]) / 2, bounds[1] + (bounds[3] - bounds[1]) / 2)
      return middle;
    },
    toJSONObject: function(){
        var ret = _.extend({}, DIAGRAMO.Paintable.prototype.toJSONObject.call(this),{
            paintDelegate: this.paintDelegate == null ? null : this.paintDelegate.toJSONObject(),
            isSelectable: this.isSelectable,
            visible: this.isVisible(),
            paintables: [],
            zIndex: this.zIndex,
            oType: "DIAGRAMO.Primitive"
        });
        this.paintables.forEach(function(p){
          ret.paintables.push(p.toJSONObject());
        });


        return ret;
    },
    paint: function(context){
        if(this.trigger("beforePaint")){
          return;
        }
        if(this.paintDelegate != null){
          this.paintDelegate.style = this.style;
          context.save();
          this.paintDelegate.paint(context);
          context.restore();
        }
        else{
          context.save();
          if(this.style){
              this.style.setupContext(context);
          }
          var self = this;
          this.paintables.forEach(function(paintable){
              context.save();
              var oldStyle = null;
              if(paintable.style){ //save primitive's style
                  oldStyle = paintable.style.clone();
              }

              if(paintable.style == null){ //if primitive does not have a style use Figure's one
                  paintable.style = self.style.clone();
              }
              else{ //if primitive has a style merge it
                  paintable.style.merge(self.style);
              }
              paintable.paint(context);
              paintable.style = oldStyle;
              context.restore();
          });
          context.restore();
        }
    },
    disable: function(handlerName){
        this._disabledHandlers.push(handlerName);
        this.paintables.forEach(function(paintable){
            paintable.disable(handlerName);
        });
    },
    enable: function(handlerName){
        this._disabledHandlers = _.without(this._disabledHandlers, handlerName);
        this.paintables.forEach(function(paintable){
            paintable.enable(handlerName);
        });
    },
    click: function(eventHandler){
        if(_.isUndefined(this.clickHandlers) || _.isNull(this.clickHandlers)){
            this.clickHandlers = [];
        }
        this.clickHandlers.push(eventHandler);
    },
    mouseOut: function(eventHandler){
        if(_.isUndefined(this.mouseOutHandlers) || _.isNull(this.mouseOutHandlers)){
            this.mouseOutHandlers = [];
        }
        this.mouseOutHandlers.push(eventHandler);
    },
    mouseOver: function(eventHandler){
        if(_.isUndefined(this.mouseOverHandlers) || _.isNull(this.mouseOverHandlers)){
            this.mouseOverHandlers = [];
        }
        this.mouseOverHandlers.push(eventHandler);
    },
    mouseMove: function(eventHandler){
        if(_.isUndefined(this.mouseMoveHandlers) || _.isNull(this.mouseMoveHandlers)){
            this.mouseMoveHandlers = [];
        }
        this.mouseMoveHandlers.push(eventHandler);
    },
    mouseDown: function(eventHandler){
        if(_.isUndefined(this.mouseDownHandlers) || _.isNull(this.mouseDownHandlers)){
            this.mouseDownHandlers = [];
        }
        this.mouseDownHandlers.push(eventHandler);
    },
    mouseUp: function(eventHandler){
        if(_.isUndefined(this.mouseUpHandlers) || _.isNull(this.mouseUpHandlers)){
            this.mouseUpHandlers = [];
        }
        this.mouseUpHandlers.push(eventHandler);
    },
    keyDown: function(eventHandler){
        if(_.isUndefined(this.keyDownHandlers) || _.isNull(this.keyDownHandlers)){
            this.keyDownHandlers = [];
        }
        this.keyDownHandlers.push(eventHandler);
    },
    keyUp: function(eventHandler){
        if(_.isUndefined(this.keyUpHandlers) || _.isNull(this.keyUpHandlers)){
            this.keyUpHandlers = [];
        }
        this.keyUpHandlers.push(eventHandler);
    },
    beforeSelect: function(eventHandler){
        if(_.isUndefined(this.beforeSelectHandlers) || _.isNull(this.beforeSelectHandlers)){
            this.beforeSelectHandlers = [];
        }
        this.beforeSelectHandlers.push(eventHandler);
    },
    beforePaint: function(eventHandler){
        if(_.isUndefined(this.beforePaintHandlers) || _.isNull(this.beforePaintHandlers)){
            this.beforePaintHandlers = [];
        }
        this.beforePaintHandlers.push(eventHandler);
    },
    beforeSelect: function(eventHandler){
        if(_.isUndefined(this.beforeSelectHandlers) || _.isNull(this.beforeSelectHandlers)){
            this.beforeSelectHandlers = [];
        }
        this.beforeSelectHandlers.push(eventHandler);
    },
    beforeTransform: function(eventHandler){
        if(_.isUndefined(this.beforeTransformHandlers) || _.isNull(this.beforeTransformHandlers)){
            this.beforeTransformHandlers = [];
        }
        this.beforeTransformHandlers.push(eventHandler);
    },
    afterTransform: function(eventHandler){
        if(_.isUndefined(this.afterTransformHandlers) || _.isNull(this.afterTransformHandlers)){
            this.afterTransformHandlers = [];
        }
        this.afterTransformHandlers.push(eventHandler);
    },
    trigger: function(eventName, args){
        var self = this;
        var ret = false;
        if(_.indexOf(this._disabledHandlers, eventName) >= 0){
            return ret;
        }
        if(!_.isNull(this[eventName + "Handlers"]) && !_.isUndefined(this[eventName + "Handlers"])){
            this[eventName + "Handlers"].some(function(eventHandler){
              var oldRet = ret;
               ret = eventHandler.apply(self, args);
               if(oldRet && eventName.indexOf("before") != -1){
                 ret = oldRet;
               }
               if(eventName.indexOf("before") == -1){
                 return ret;
               }
            });
        }
        return ret;
    }
});
