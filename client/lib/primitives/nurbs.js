/**
 * Implements a DIAGRAMO.NURBS component in Diagramo
 * @param {Array} points - an {Array} of {Point}s
 * @see http://en.wikipedia.org/wiki/Non-uniform_rational_B-spline
 *
 * http://www.w3.org/Graphics/SVG/IG/resources/svgprimer.html#path_Q
 * http://math.stackexchange.com/questions/92246/aproximate-n-grade-bezier-through-cubic-and-or-quadratic-bezier-curves
 * @see http://stackoverflow.com/questions/1257168/how-do-i-create-a-bezier-curve-to-represent-a-smoothed-polyline
 * @see http://www.codeproject.com/KB/graphics/BezierSpline.aspx Draw a Smooth Curve through a Set of 2D DIAGRAMO.Points with Bezier Primitives
 *  "Paul de Casteljau, a brilliant engineer at Citroen"
 * @see http://stackoverflow.com/questions/8369488/splitting-a-bezier-curve
 * @see http://devmag.org.za/2011/04/05/bzier-curves-a-tutorial/
 * @see http://devmag.org.za/2011/06/23/bzier-path-algorithms/
 * @see http://drdobbs.com/cpp/184403417 (Forward Difference Calculation of Bezier Curves)
 * @see http://www.timotheegroleau.com/Flash/articles/cubic_bezier_in_flash.htm
 * @see http://www.algorithmist.net/bezier3.html
 * @see http://www.caffeineowl.com/graphics/2d/vectorial/bezierintro.html
 * @author Alex Gheorghiu <alex@scriptoid.com>
 **/
function NURBS(points){
    DIAGRAMO.Primitive.call(this);
    if(points.length < 2){
        throw "NURBS: contructor() We need minimum 3 points to have a DIAGRAMO.NURBS";
    }

    /**The initial {@link DIAGRAMO.Point}s*/
    this.points = points;

    /**The array of {CubicCurve} s from which the NURB will be made*/
    this.fragments = this.nurbsPoints(DIAGRAMO.Point.cloneArray(this.points));

    /**The {@link DIAGRAMO.Style} of the line*/
    this.style = new DIAGRAMO.Style();
    this.style.gradientBounds = this.getBounds();

    /**Serialization type*/
    this.oType = 'NURBS'; //object type used for JSON deserialization
}

DIAGRAMO.NURBS = NURBS;

/**Creates a {NURBS} out of JSON parsed object
 *@param {JSONObject} o - the JSON parsed object
 *@return {NURBS} a newly constructed DIAGRAMO.NURBS
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
NURBS.load = function(o){

    var newNURBS = new DIAGRAMO.NURBS(DIAGRAMO.Point.loadArray(o.points));
    newNURBS.style = DIAGRAMO.Style.load(o.style);
    DIAGRAMO.Primitive.load(o, newNURBS);
    return newNURBS;
};

NURBS.prototype = _.extend({},  DIAGRAMO.Primitive.prototype, {
    toJSONObject: function(){
        var ret = _.extend({}, DIAGRAMO.PointBasedPrimitive.prototype.toJSONObject.call(this),{
            oType: "DIAGRAMO.NURBS",
            points: DIAGRAMO.Point.toJSONArray(this.points),
        });
        return ret;
    },
    /**Computes a series of Bezier(Cubic) curves to aproximate a curve modeled
     *by a set of points
     *@param {Array}- P and {Array} of {Point}s
     *@return an {Array} of {CubicCurve} (provided also as {Array})
     *Example:
     *  [
     *      [p1, p2, p3, p4],
     *      [p1', p2', p3', p4'],
     *      etc
     *  ]
     * See /documents/specs/spline-to-bezier.pdf (pages 5 and 6 for a description)
     **/
    nurbsPoints : function (P){
        var n = P.length;

        /**Contains the gathered sub curves*/
        var sol = [];

        if(n === 2){
            sol.push(new DIAGRAMO.Line(P[0], P[1]));
            return sol;
        }
        else if(n === 3){
            sol.push(new DIAGRAMO.QuadCurve(P[0], P[1], P[2]));
            return sol;
        }
        else if(n === 4){
            sol.push(new DIAGRAMO.CubicCurve(P[0], P[1], P[2], P[3]));
            return sol;
        }

        /**Computes factorial
         * @param {Number} k the number
         * */
        function fact(k){
            if(k===0 || k===1){
                return 1;
            }
            else{
                return k * fact(k-1);
            }
        }

        /**Computes Bernstain*/
        function B(i,n,u){
            return fact(n) / (fact(i) * fact(n-i))* Math.pow(u, i) * Math.pow(1-u, n-i);
        }

        /**Computes the sum between two points
         *@param p1 - {Point}
         *@param p2 - {Point}
         *@return {Point} the sum of initial points
         **/
        function sum(p1, p2){
            return new DIAGRAMO.Point(p1.x + p2.x, p1.y + p2.y);
        }

        /**Computes the difference between first {Point} and second {Point}
         *@param p1 - {Point}
         *@param p2 - {Point}
         *@return {Point} the sum of initial points
         **/
        function minus(p1, p2){
            return new DIAGRAMO.Point(p1.x - p2.x, p1.y - p2.y);
        }

        /**Computes the division of a {Point} by a number
         *@param p - {Point}
         *@param nr - {Number}
         *@return {Point}
         **/
        function divide(p, nr){
            if(nr == 0){
                throw "Division by zero not allowed (yet :) " + this.callee ;
            }
            return new DIAGRAMO.Point(p.x/nr, p.y/nr);
        }

        /**Computes the multiplication of a {Point} by a number
         *@param p - {Point}
         *@param nr - {Number}
         *@return {Point}
         **/
        function multiply(p, nr){
            return new DIAGRAMO.Point (p.x * nr, p.y * nr);
        }




        /*
         *I do not get why first 4 must be 0 and last 3 of same value.....
         *but otherwise we will get division by zero
         */
        var k = [0,0,0];

        var j;
        for(j=0;j<=n-3;j++){
            k.push(j);
        }

        k.push(n-3, n-3);



        for(i=1; i<=n-3; i++){
            //q1 - compute start point
            var q1 = divide( sum( multiply(P[i], k[i+4] - k[i+2]), multiply(P[i+1], k[i+2] - k[i+1]) ), k[i+4] - k[i+1]);

            //q0 - compute 1st controll point
            var q_01 = (k[i+3] - k[i+2]) / (k[i+3] - k[i+1]);
            var q_02 = divide( sum( multiply(P[i-1],k[i+3] - k[i+2]), multiply(P[i], k[i+2] - k[i])), k[i+3] - k[i]);
            var q_03 = multiply(q1, ( k[i+2] - k[i+1])/ (k[i+3] - k[i+1]) );
            var q0 = sum(multiply(q_02, q_01), q_03);

            //q2 - compute 2nd controll point
            var q2 = divide( sum( multiply(P[i], k[i+4] - k[i+3]), multiply(P[i+1], k[i+3] - k[i+1]) ), k[i+4] - k[i+1] );

            //q3 - compute end point
            var q_31 = (k[i+3] - k[i+2]) / (k[i+4] - k[i+2]);
            var q_32 = divide( sum( multiply(P[i+1], k[i+5] - k[i+3]), multiply(P[i+2], k[i+3] - k[i+2]) ) , k[i+5] - k[i+2]);
            var q_33 = multiply(q2, (k[i+4] - k[i+3])/(k[i+4] - k[i+2]) );
            var q3 = sum(multiply(q_32, q_31), q_33);

            //store solution
            sol.push( new DIAGRAMO.CubicCurve(q0, q1, q2, q3) );
        }

        return sol;
    },

    /**Paint the DIAGRAMO.NURBS*/
    paint : function(context, delegate){
        context.beginPath();

        if(this.style != null){
            this.style.setupContext(context);
        }

        //Log.info("Nr of cubic curves " +  this.fragments.length);
        for(var f=0; f<this.fragments.length; f++){
            var fragment = this.fragments[f];
            fragment.style = this.style.clone();
            var length = fragment.getLength();

            if(delegate != undefined) {
                var length = fragment.getLength();
                var points = DIAGRAMO.Util.equidistancePoints(fragment, length < 100 ? (length < 50 ? 3: 5): 11);
                points.splice(0, 1);
                //points.splice(0, 1);
                delegate.paint(context, points);
            }
            else {
                fragment.paint(context);
            }
        }
    },


    transform : function(matrix){
        //transform cubic curves
        this.points.forEach(function(point){
            point.transform(matrix);
        })
        for(var f=0; f<this.fragments.length; f++){
            var fragment = this.fragments[f];
            fragment.transform(matrix);
        }
    },


    /** Tests to see if a point belongs to this DIAGRAMO.NURBS
     * @param {Number} x - the X coordinates
     * @param {Number} y - the Y coordinates
     * @author Alex Gheorghiu <alex@scriptoid.com>
     **/
    contains: function(x, y){
        for(var f=0; f<this.fragments.length; f++){
            var fragment = this.fragments[f];
            if(fragment.contains(x, y)){
                return true;
            }
        }

        return false;
    },

    /**Computes the length of the {NURB} by summing the length of {CubicCurve}s
     * is made of.
     * TODO: as this involves a lot of computations it would nice to use a Math formula
     * */
    getLength : function(){
        var l = 0;

        for(var ci=0; ci<this.fragments.length; ci++){
            l += this.fragments[ci].getLength();
        }
        return l;
    },

    getMiddle : function() {
        var points = this.getPoints();
        var poly = new DIAGRAMO.Polyline();
        poly.points = points;

        return poly.getVisualPoint(0.5);
    },

    equals : function (object){
        throw Exception("Not implemented");
    },

    toString : function(){
        throw Exception("Not implemented");
    },


    toSVG : function() {

        var result = "\n" + repeat("\t", INDENTATION) +  '<path d="';

        for(var f=0; f<this.fragments.length; f++){
            var fragment = this.fragments[f];

            result += 'M' + fragment.startPoint.x + ',' + fragment.endPoint.y;
            result += ' C' + fragment.controlPoint1.x + ',' + fragment.controlPoint1.y;
            result += ' ' + fragment.controlPoint2.x + ',' + fragment.controlPoint2.y;
            result += ' ' + fragment.endPoint.x + ',' + fragment.endPoint.y;
        }

        result += '" style="' + this.style.toSVG() +  '"  />';

        return result;
    },

    clone : function() {
        var ret = new NURBS(DIAGRAMO.Point.cloneArray(this.points));
        ret.fragments = this.fragments.map(function(frag){
            return frag.clone();
        });
        return ret;
    },

    getBounds: function(){
        return DIAGRAMO.Util.getBounds(this.getPoints());
    },

    near : function(x, y, radius){
        for(var f=0; f<this.fragments.length; f++){
            var fragment = this.fragments[f];
            if(fragment.near(x, y, radius)){
                return true;
            }
        }

        return false;
    },

    getPoints : function(){
        var points = [];
        for(var f=0; f < this.fragments.length; f++){
            var fragment = this.fragments[f];
            points = points.concat(fragment.getPoints());
        }
        return points;
    }
});
