DIAGRAMO.Image = function(startPoint, src){
    DIAGRAMO.Polygon.call(this);
    var self = this;
    this.addPoint(startPoint);
    this.addPoint(new DIAGRAMO.Point(startPoint.x, startPoint.y));
    this.addPoint(new DIAGRAMO.Point(startPoint.x, startPoint.y));
    this.addPoint(new DIAGRAMO.Point(startPoint.x, startPoint.y));
    this.image = new Image();
    this.flippedV = false;
    this.flippedH = false;

    this.image.onload = function(){
      if(!self.ready){
        self.setupPoints();
      }
    }
    this.image.onerror =function(e){
      self.ready = true;
      console.log(e);
    }
    if(DIAGRAMO.TEST){
      this.image.src = src.replace(/http:\/\/.*\/globals/,DIAGRAMO.TEST + "/globals");
    }
    else{
      this.image.src = src.replace(/http:\/\/.*\/globals/,"http://" + window.location.hostname + "/globals");
      //this.image.src = src;
    }
    if(this.image.complete){
      this.image.onload();
    }
}

DIAGRAMO.Image.load = function(obj){
    var img;
    if(obj.startPoint){
      img = new DIAGRAMO.Image(DIAGRAMO.Point.load(obj.startPoint), obj.imageSrc);
      obj.points = [];
      obj.points.push(img.points[0].toJSONObject());
      obj.points.push(img.points[1].toJSONObject());
      obj.points.push(img.points[2].toJSONObject());
      obj.points.push(img.points[3].toJSONObject());
      obj.style = img.style;
    }
    else{
      img = new DIAGRAMO.Image(DIAGRAMO.Point.load(obj.points[0]), obj.imageSrc);
    }
    img = DIAGRAMO.Polygon.load(obj, img);
    if(DIAGRAMO.Util.distance(img.points[0], img.points[1]) != 0){
      img.image.width = DIAGRAMO.Util.distance(img.points[0], img.points[1]);
      img.image.height = DIAGRAMO.Util.distance(img.points[1], img.points[2]);
    }
    return img;
}

_.extend(DIAGRAMO.Image.prototype, DIAGRAMO.Polygon.prototype, {
    transform: function(matrix){
      if(this.ready){
        DIAGRAMO.Polygon.prototype.transform.call(this, matrix);
        this.image.width = DIAGRAMO.Util.distance(this.points[0], this.points[1]);
        this.image.height = DIAGRAMO.Util.distance(this.points[1], this.points[2]);
        if(matrix[0][0] == -1){
          this.flippedH = !this.flippedH;
        }
        else if(matrix[1][1] == -1){
          this.flippedV = !this.flippedV;
        }
      }
    },
    toJSONObject: function(){
        return _.extend(DIAGRAMO.Polygon.prototype.toJSONObject.call(this), {
            oType: "DIAGRAMO.Image",
            imageSrc: this.image.src,
        });
    },
    setupPoints: function(){
      var self = this;
      self.ready = true;
      if(self.image.width == 0 && self.image.height == 0){
        self.image.width = DIAGRAMO.PROPERTIES.MAX_IMAGE_WIDTH;
        self.image.height = DIAGRAMO.PROPERTIES.MAX_IMAGE_HEIGHT;
      }
      if(self.image.width > DIAGRAMO.PROPERTIES.MAX_IMAGE_WIDTH || self.image.height > DIAGRAMO.PROPERTIES.MAX_IMAGE_HEIGHT){

        if(self.image.width > self.image.height){
          var ratio = self.image.width / self.image.height;
          self.image.width = DIAGRAMO.PROPERTIES.MAX_IMAGE_WIDTH;
          self.image.height = DIAGRAMO.PROPERTIES.MAX_IMAGE_WIDTH / ratio;
        }
        else{
          var ratio = self.image.height / self.image.width;
          self.image.height = DIAGRAMO.PROPERTIES.MAX_IMAGE_HEIGHT;
          self.image.width = DIAGRAMO.PROPERTIES.MAX_IMAGE_HEIGHT / ratio;
        }
      }
      if(self.points[1].x == self.points[0].x){
        self.points[1].x = self.points[0].x + self.image.width;
        self.points[2].x = self.points[0].x + self.image.width;
        self.points[2].y = self.points[0].y + self.image.height;
        self.points[3].y = self.points[0].y + self.image.height;
      }
    },
    paint: function(context){
      var self = this;
      if(!this.ready){
        window.setTimeout(function(){
          _.keys(DIAGRAMO.editors).forEach(function(e){
            DIAGRAMO.editors[e].repaint();
          })
        }, 100);
          return;
      }
      var angle2 = DIAGRAMO.Util.getAngle(this.points[3], this.points[0]);
      if(this.flippedV){
        this.points[3].transform(DIAGRAMO.Matrix.flipVerticalMatrix());
        this.points[0].transform(DIAGRAMO.Matrix.flipVerticalMatrix());
      }
      if(this.flippedH){
        this.points[3].transform(DIAGRAMO.Matrix.flipHorizontalMatrix());
        this.points[0].transform(DIAGRAMO.Matrix.flipHorizontalMatrix());
      }
      var angle = DIAGRAMO.Util.getAngle(this.points[3], this.points[0]);
      if(this.flippedV){
        this.points[3].transform(DIAGRAMO.Matrix.flipVerticalMatrix());
        this.points[0].transform(DIAGRAMO.Matrix.flipVerticalMatrix());
      }
      if(this.flippedH){
        this.points[3].transform(DIAGRAMO.Matrix.flipHorizontalMatrix());
        this.points[0].transform(DIAGRAMO.Matrix.flipHorizontalMatrix());
      }
      var centerPoint = DIAGRAMO.Util.getCenter(this.getBounds());
      var oldWidth = this.image.width;
      var oldHeight = this.image.height;
      this.transform(DIAGRAMO.Matrix.translationMatrix(-centerPoint.x, -centerPoint.y));
      this.transform(DIAGRAMO.Matrix.rotationMatrix(-angle2));
      this.transform(DIAGRAMO.Matrix.translationMatrix(centerPoint.x, centerPoint.y));
      var startPoint = this.points[0].clone();
      this.transform(DIAGRAMO.Matrix.translationMatrix(-centerPoint.x, -centerPoint.y));
      this.transform(DIAGRAMO.Matrix.rotationMatrix(angle2));
      this.transform(DIAGRAMO.Matrix.translationMatrix(centerPoint.x, centerPoint.y));
      this.image.width = oldWidth;
      this.image.height = oldHeight;

      context.save();
      context.translate(startPoint.x, startPoint.y);
      if(this.flippedH){
        context.scale(-1, 1);
      }
      if(this.flippedV){
        context.scale(1, -1);
        //do not understand the need for this at all.
        context.translate(-this.image.width, -this.image.height);
      }
      context.translate(this.image.width / 2, this.image.height / 2);
      context.rotate(angle);
      try{
        context.drawImage(this.image,  -this.image.width / 2, -this.image.height / 2, this.image.width, this.image.height);
      }
      catch(e){
        console.log(e);
      }
      context.restore();
    },
});
