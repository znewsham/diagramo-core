function Paintable(){
    this.style = new DIAGRAMO.Style();
}
DIAGRAMO.Paintable = Paintable;

_.extend(DIAGRAMO.Paintable.prototype, {
    getCursor: function(event){
        return "";
    },
    toJSONObject: function(){
        var ret = {
            style: this.style.clone(),
            id: this.id,
        };

        return ret;
    },
    isVisible: function(){
        return this.visible == true || _.isUndefined(this.visible);
    },
    getBounds:function(){
        return DIAGRAMO.Util.getBounds(this.getPoints());
    },
});
