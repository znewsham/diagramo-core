function Diagramo(canvas){
    this.canvas = canvas;
    this.origWidth = this.canvas.width;
    this.origHeight = this.canvas.height;
    this.paintables = [];
    this.debug = false;
    this.scale = 1;
    var self = this;
    this.overPaintables = [];
    this.overPaintable = null;
    self.inMove = false;
    self.mouseX = 0;
    self.mouseY = 0;
    this.canvas.oncontextmenu = function (e) {
     e.preventDefault();
     return false;
   };
    this.canvas.onmouseover = function(event){
        self.mouseHandler.call(self, event, "mouseOverHandler");
    };
    this.canvas.onmousemove = function(event){
        self.mouseHandler.call(self, event, "mouseMoveHandler");
    };
    this.canvas.onclick = function(e){
        $("canvas")[0].focus();
        e.stopPropagation();
        e.bubbles = false;
    };
    this.canvas.onmousedown = function(e){
        $("canvas")[0].focus();
        e.stopPropagation();
        e.bubbles = false;
        self.mouseHandler.call(self, e, "mouseDownHandler");
    };
    this.canvas.onmouseup = function(event){
        $("canvas")[0].focus();
        self.mouseHandler.call(self, event, "mouseUpHandler");
    };
    document.onkeydown = function(event){
        self.keyHandler.call(self, event, "keyDownHandler");
    }
    document.onkeyup = function(event){
        self.keyHandler.call(self, event, "keyUpHandler");
    }
}


_.extend(Diagramo.prototype, {
    setScale: function(scale){
      $(this.canvas).attr("width", this.origWidth * scale);
      $(this.canvas).attr("height", this.origHeight * scale);
      var ctx = this.canvas.getContext("2d");
      ctx.scale(scale, scale);
      this.scale = scale;
      this.repaint();
    },
    getMousePosition: function(){
      return {x: this.mouseX, y: this.mouseY}
    },
    load: function(jsonObject){
        var self = this;
        this.origWidth = this.canvas.width;
        this.origHeight = this.canvas.height;
    },
    getSortedPaintables: function(){
      return _.sortBy(this.paintables, function(paintable, index){
          return _.isUndefined(paintable.zIndex) ? index + 1000 : paintable.zIndex;
      });
    },
    paint: function(context){
      context.paintLater = [];
       context.clearRect(0,0,this.canvas.width,this.canvas.height);
        var paintables = this.getSortedPaintables();
        paintables.reverse().forEach(function(paintable){
            if(paintable.isVisible()){
                paintable.paint(context);
            }
        });
        if(context.paintLater){
          context.paintLater.forEach(function(paintable){
            context.save();
            paintable.paint(context);
            context.restore();
          })
        }
    },

    getContext: function(){
      return this.canvas.getContext('2d');
    },
    repaint: function(){
      if(this.painting){
        return;
      }
      this.painting = true;
      this.paint(this.canvas.getContext('2d'));
      this.painting = false;
    },

    addPaintable: function(paintable){
        this.paintables.push(paintable);
        DIAGRAMO.paintableEditorMap[paintable.id] = this;
    },
    removePaintable: function(paintable){
        this.paintables = _.without(this.paintables, paintable);
    },

    generateId: function(){
        return DIAGRAMO.generateId();
    },

    keyHandler: function(event, eventName){
        var self = this;
        if((DIAGRAMO.isCtrlPressed() && (
            event.keyCode == 82 ||
            event.keyCode == 189 || //minus
            event.keyCode == 187 || //plus?
            event.keyCode == 109 ||
            event.keyCode == 107)
          ) ||
          event.target.tagName == "INPUT" ||
          event.target.tagName == "TEXTAREA"){
          return
        }

        event.preventDefault();
        if(this.selectedObject){
          var ret = this.selectedObject.trigger(eventName.replace("Handler",""),[{
              oType: eventName.replace("Handler",""),
              keyCode: event.keyCode,
              event: event
          }]);
          if(ret){
            this.repaint();
            return;
          }
        }
        if(this[eventName]){
            this[eventName].call(this, {
                oType: eventName.replace("Handler",""),
                keyCode: event.keyCode,
                event: event
            });
        }
    },
    canvasGetXY: function(x, y){
      //NOTE: handles the case where the canvas is hidden (almost exclusively tests)
      var elem = $(this.canvas);
      const changes = [];
      while($(this.canvas).width() === 0 && elem.parent()){
        if(elem.css('display') == 'none'){
          changes.push(elem);
          elem.css("display","auto");
        }
        elem = elem.parent();
      }
      var ret =  {x: (x - $(this.canvas).offset().left) / this.scale, y: (y - $(this.canvas).offset().top) / this.scale}

      changes.forEach(function(elem){
        elem.css("display","none");
      });
      return ret;
    },
    mouseHandler: function(event, eventName){
        if(event.ctrlKey === undefined){
          debugger;
        }
        this.ctrlPressed = DIAGRAMO.ctrlPressed = event.ctrlKey;
        this.shiftPressed = DIAGRAMO.shiftPressed = event.shiftKey;
        var self = this;
        var x = event.offsetX;
        var y = event.offsetY;
        x /= this.scale;
        y /= this.scale;
        this.mouseX = x;
        this.mouseY = y;
        var paintable = this.getPaintableByXY(x, y, "isHidden", false);//(eventName == "clickHandler" || eventName == "mouseDownHandler") ? "isSelectable" :
        var bubbleBlocked = false;
        var origEventName = eventName;
        self.canvas.style.cursor = paintable ? paintable.getCursor({x: x, y:y}) : "";
        var oldOver = this.overPaintable;
        var oldOvers = this.overPaintables;
        this.overPaintables = this.getPaintablesByXY(x, y, "isHidden", false);
        this.overPaintables.forEach(function(p){
          if(oldOvers.indexOf(p) == -1){
            p.trigger("mouseOver", [{
                oType: "mouseOver",
                x: x,
                y: y,
                paintable: p,
                diagramo: self,
                event: event
            }]);
          }
        });
        oldOvers.forEach(function(p){
          if(self.overPaintables.indexOf(p) == -1){
            p.trigger("mouseOut", [{
                oType: "mouseOut",
                x: x,
                y: y,
                paintable: p,
                diagramo: self,
                event: event
            }]);
          }
        });

        if(paintable && paintable[eventName + "s"]){
            bubbleBlocked = paintable[eventName + "s"].some(function(eventHandler){
                var ret = eventHandler.call(paintable, {
                    oType: eventName.replace("Handler",""),
                    x: x,
                    y: y,
                    paintable: paintable,
                    diagramo: self,
                    event: event
                });
                if(ret == undefined){
                    ret = false;
                }
                return ret;
            });
        }
        if((eventName == "mouseOverHandler" || paintable == null) && !bubbleBlocked && oldOver && oldOver["mouseOutHandlers"]){
          oldOver["mouseOutHandlers"].some(function(eventHandler){
              var ret = eventHandler.call(self.overPaintable, {
                  oType: "mouseOut",
                  x: x,
                  y: y,
                  paintable: self.overPaintable,
                  diagramo: self,
                  event: event
              });
              if(ret == undefined){
                  ret = false;
              }
              return ret;
          });
        }
        if(!bubbleBlocked && !_.isUndefined(this[eventName])){
            this[eventName].call(this, {
                oType: origEventName.replace("Handler",""),
                x: x,
                y: y,
                paintable: paintable
            });
        }
    },

    getPaintableById: function(id){
        var ret = null
        this.paintables.some(function(paintable){
            if(paintable.id == id){
                ret = paintable;
                return true;
            }
        });
        return ret;
    },

    getPaintablesByXY: function(x, y, propertyCheck, propRet){
        var ret = [];
        var paintables = this.getSortedPaintables();
        paintables.forEach(function(paintable){
           var check = (propertyCheck == undefined || (propRet ? paintable[propertyCheck] : !paintable[propertyCheck]));
           if(check && paintable.contains(x, y) && paintable.isVisible()){
               ret.push(paintable);
           }
        });
        return ret;
    },
    getPaintableByXY: function(x, y, propertyCheck, propRet){
        var ret = null;
        var paintables = this.getSortedPaintables();
        paintables.forEach(function(paintable){
           var check = (propertyCheck == undefined || (propRet ? paintable[propertyCheck] : !paintable[propertyCheck]));
           if(check && ret == null && paintable.contains(x, y) && paintable.isVisible()){
               ret = paintable;
           }
        });
        return ret;
    },
    getPaintablesIntersectingBounds: function(bounds){
       var lineAB = new DIAGRAMO.Line(new DIAGRAMO.Point(bounds[0], bounds[1]), new DIAGRAMO.Point(bounds[2], bounds[1]));
       var lineBC = new DIAGRAMO.Line(new DIAGRAMO.Point(bounds[2], bounds[1]), new DIAGRAMO.Point(bounds[2], bounds[3]));
       var lineCD = new DIAGRAMO.Line(new DIAGRAMO.Point(bounds[2], bounds[3]), new DIAGRAMO.Point(bounds[0], bounds[3]));
       var lineDA = new DIAGRAMO.Line(new DIAGRAMO.Point(bounds[0], bounds[3]), new DIAGRAMO.Point(bounds[0], bounds[1]));
       var ret = [];
       function intersects(paintable, bounds){
         if(!paintable.getPoints || paintable.isText){
           return true;
         }
         var points = paintable.getPoints();
         for(var i = 0; i < points.length - 1; i++){
           if(DIAGRAMO.Util.lineIntersectsRectangle(points[i], points[i + 1], bounds)){
             return true;
           }
         }
         return false;
       }
       this.paintables.forEach(function(paintable){
           var pBounds = paintable.getBounds();
            if(
                paintable.isVisible() && !paintable.isHidden &&
                (DIAGRAMO.Util.areBoundsInBounds(pBounds, bounds) ||
                ((DIAGRAMO.Util.lineIntersectsRectangle(lineAB.startPoint, lineAB.endPoint, pBounds) ||
                DIAGRAMO.Util.lineIntersectsRectangle(lineBC.startPoint, lineBC.endPoint, pBounds) ||
                DIAGRAMO.Util.lineIntersectsRectangle(lineCD.startPoint, lineCD.endPoint, pBounds) ||
                DIAGRAMO.Util.lineIntersectsRectangle(lineDA.startPoint, lineDA.endPoint, pBounds)  ||
                DIAGRAMO.Util.areBoundsInBounds(bounds, pBounds)) && intersects(paintable, bounds)))){
                    ret.push(paintable);
            }
       });
       return ret;
    }
});

DIAGRAMO = {
    shiftPressed: false,
    ctrlPressed: false,
    altPressed: false,
    currentId: 0,
    widgets: {

    },
    Diagramo: Diagramo,
    load: function(jsonObject){
        if(jsonObject == null){
            return null;
        }
        var parts = (jsonObject.oType + ".load").split(".");
        var object = window;
        try {
          for(var i = 0; i < parts.length; i++){
              object = object[parts[i]];
          }
        }
        catch(e){
          console.log(jsonObject);
          console.log(e);
          if(jsonObject.oType.indexOf("DIAGRAMO") != 0){
            jsonObject.oType = "DIAGRAMO." + jsonObject.oType;
            return DIAGRAMO.load(jsonObject);
          }
          else{
            return {};
          }
        }
        return object(jsonObject);
    },
    generateId: function(){
        return ++DIAGRAMO.currentId;
    },
    isShiftPressed: function(){
        return DIAGRAMO.shiftPressed;
    },
    isCtrlPressed: function(){
        return DIAGRAMO.ctrlPressed;
    },
    isAltPressed: function(){
        return DIAGRAMO.altPressed;
    },
    paintableEditorMap: {

    },
    editorFromPaintable: function(paintable){
        return DIAGRAMO.paintableEditorMap[paintable.id];
    }
}
