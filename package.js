Package.describe({
  name: 'znewsham:diagramo-core',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
    api.versionsFrom('1.2.1');
    api.use('ecmascript');
    api.use(['templating'], 'client');
    api.use('underscore');

    api.addFiles('client/lib/diagramo-core.js');
    api.export("DIAGRAMO", ['client','server']);
    api.addFiles([
        "client/lib/paintable.js",
        "client/lib/style.js",
        "client/lib/util.js",
        "client/lib/matrix.js",
        "client/lib/primitives/primitive.js",
        "client/lib/primitives/pointBasedPrimitive.js",
        "client/lib/primitives/point.js",
        "client/lib/primitives/line.js",
        "client/lib/primitives/polyline.js",
        "client/lib/primitives/path.js",
        "client/lib/primitives/polygon.js",
        "client/lib/primitives/curve.js",
        "client/lib/primitives/quadCurve.js",
        "client/lib/primitives/cubicCurve.js",
        "client/lib/primitives/nurbs.js",
        "client/lib/primitives/arc.js",
        "client/lib/primitives/ellipse.js",
        "client/lib/primitives/text.js",
        "client/lib/primitives/image.js"
    ],["client"]);
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('znewsham:diagramo-core');
  api.addFiles('diagramo-core-tests.js');
});
